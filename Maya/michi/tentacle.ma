//Maya ASCII 2014 scene
//Name: tentacle.ma
//Last modified: Sun, May 25, 2014 01:03:09 AM
//Codeset: 1252
requires maya "2014";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2014";
fileInfo "version" "2014";
fileInfo "cutIdentifier" "201307170459-880822";
fileInfo "osv" "Microsoft Windows 8 , 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 5.1962491256166432 4.1715800658661832 -6.7770081209696924 ;
	setAttr ".r" -type "double3" -17.13835268312549 -932.60000000032448 0 ;
	setAttr ".rp" -type "double3" -2.2204460492503131e-016 0 0 ;
	setAttr ".rpt" -type "double3" 2.0732884289296211e-016 0 -2.2155643282046394e-016 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999979;
	setAttr ".coi" 8.2600676334212775;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -0.24807866576427795 2.7411342527413742 -1.5670081085069092 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30.584210526315797;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 2.1747443588449822 -0.085948022727921569 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 11.960979821373297;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "m_tentacle";
	setAttr ".t" -type "double3" 0 5 0 ;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode mesh -n "m_tentacleShape" -p "m_tentacle";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.45595671236515045 0.65644457936286926 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".vcs" 2;
createNode mesh -n "m_tentacleShape1Orig" -p "m_tentacle";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode joint -n "joint1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "none";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 -0.007165049268386783 -0.0048754599062838392 1;
	setAttr ".radi" 0.5;
createNode joint -n "joint2" -p "joint1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 0 0.91647244928832938 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "none";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0.90930740001994259 -0.0048754599062838392 1;
	setAttr ".radi" 0.5438133411274787;
createNode joint -n "joint3" -p "joint2";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".t" -type "double3" 2.9873315002025819e-021 1.8470444747195567 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "none";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 2.9873315002025819e-021 2.7563518747394991 0.0021743281651649413 1;
	setAttr ".radi" 0.53879559416320999;
createNode joint -n "joint4" -p "joint3";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 3.8662310529346764e-016 1.7500468030864278 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "none";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 3.8662609262496784e-016 4.5063986778259268 2.2161092405603711e-016 1;
	setAttr ".radi" 0.53879559416320999;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	setAttr ".cdl" 2;
	setAttr -s 3 ".dli[1:2]"  1 2;
	setAttr -s 3 ".dli";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode polyCone -n "polyCone1";
	setAttr ".h" 10;
	setAttr ".sa" 10;
	setAttr ".sh" 20;
	setAttr ".cuv" 3;
createNode tweak -n "tweak1";
	setAttr -s 195 ".vl[0].vt";
	setAttr ".vl[0].vt[10]" -type "float3" 0 -0.022916809 0 ;
	setAttr ".vl[0].vt[11]" -type "float3" 0 -0.022916809 0 ;
	setAttr ".vl[0].vt[12]" -type "float3" 0 -0.022916809 0 ;
	setAttr ".vl[0].vt[13]" -type "float3" 0 -0.022916809 0 ;
	setAttr ".vl[0].vt[14]" -type "float3" 0 -0.022916809 0 ;
	setAttr ".vl[0].vt[15]" -type "float3" 0 -0.022916809 0 ;
	setAttr ".vl[0].vt[16]" -type "float3" 0 -0.022916809 0 ;
	setAttr ".vl[0].vt[17]" -type "float3" 0 -0.022916809 0 ;
	setAttr ".vl[0].vt[18]" -type "float3" 0 -0.022916809 0 ;
	setAttr ".vl[0].vt[19]" -type "float3" 0 -0.022916809 0 ;
	setAttr ".vl[0].vt[20]" -type "float3" 0 -0.088718817 0 ;
	setAttr ".vl[0].vt[21]" -type "float3" 0 -0.088718817 0 ;
	setAttr ".vl[0].vt[22]" -type "float3" 0 -0.088718817 0 ;
	setAttr ".vl[0].vt[23]" -type "float3" 0 -0.088718817 0 ;
	setAttr ".vl[0].vt[24]" -type "float3" 0 -0.088718817 0 ;
	setAttr ".vl[0].vt[25]" -type "float3" 0 -0.088718817 0 ;
	setAttr ".vl[0].vt[26]" -type "float3" 0 -0.088718817 0 ;
	setAttr ".vl[0].vt[27]" -type "float3" 0 -0.088718817 0 ;
	setAttr ".vl[0].vt[28]" -type "float3" 0 -0.088718817 0 ;
	setAttr ".vl[0].vt[29]" -type "float3" 0 -0.088718817 0 ;
	setAttr ".vl[0].vt[30]" -type "float3" 0 -0.19580621 0 ;
	setAttr ".vl[0].vt[31]" -type "float3" 0 -0.19580621 0 ;
	setAttr ".vl[0].vt[32]" -type "float3" 0 -0.19580621 0 ;
	setAttr ".vl[0].vt[33]" -type "float3" 0 -0.19580621 0 ;
	setAttr ".vl[0].vt[34]" -type "float3" 0 -0.19580621 0 ;
	setAttr ".vl[0].vt[35]" -type "float3" 0 -0.19580621 0 ;
	setAttr ".vl[0].vt[36]" -type "float3" 0 -0.19580621 0 ;
	setAttr ".vl[0].vt[37]" -type "float3" 0 -0.19580621 0 ;
	setAttr ".vl[0].vt[38]" -type "float3" 0 -0.19580621 0 ;
	setAttr ".vl[0].vt[39]" -type "float3" 0 -0.19580621 0 ;
	setAttr ".vl[0].vt[40]" -type "float3" 0 -0.34154925 0 ;
	setAttr ".vl[0].vt[41]" -type "float3" 0 -0.34154925 0 ;
	setAttr ".vl[0].vt[42]" -type "float3" 0 -0.34154925 0 ;
	setAttr ".vl[0].vt[43]" -type "float3" 0 -0.34154925 0 ;
	setAttr ".vl[0].vt[44]" -type "float3" 0 -0.34154925 0 ;
	setAttr ".vl[0].vt[45]" -type "float3" 0 -0.34154925 0 ;
	setAttr ".vl[0].vt[46]" -type "float3" 0 -0.34154925 0 ;
	setAttr ".vl[0].vt[47]" -type "float3" 0 -0.34154925 0 ;
	setAttr ".vl[0].vt[48]" -type "float3" 0 -0.34154925 0 ;
	setAttr ".vl[0].vt[49]" -type "float3" 0 -0.34154925 0 ;
	setAttr ".vl[0].vt[50]" -type "float3" 0 -0.52236837 0 ;
	setAttr ".vl[0].vt[51]" -type "float3" 0 -0.52236837 0 ;
	setAttr ".vl[0].vt[52]" -type "float3" 0 -0.52236837 0 ;
	setAttr ".vl[0].vt[53]" -type "float3" 0 -0.52236837 0 ;
	setAttr ".vl[0].vt[54]" -type "float3" 0 -0.52236837 0 ;
	setAttr ".vl[0].vt[55]" -type "float3" 0 -0.52236837 0 ;
	setAttr ".vl[0].vt[56]" -type "float3" 0 -0.52236837 0 ;
	setAttr ".vl[0].vt[57]" -type "float3" 0 -0.52236837 0 ;
	setAttr ".vl[0].vt[58]" -type "float3" 0 -0.52236837 0 ;
	setAttr ".vl[0].vt[59]" -type "float3" 0 -0.52236837 0 ;
	setAttr ".vl[0].vt[60]" -type "float3" 0 -0.73382413 0 ;
	setAttr ".vl[0].vt[61]" -type "float3" 0 -0.73382413 0 ;
	setAttr ".vl[0].vt[62]" -type "float3" 0 -0.73382413 0 ;
	setAttr ".vl[0].vt[63]" -type "float3" 0 -0.73382413 0 ;
	setAttr ".vl[0].vt[64]" -type "float3" 0 -0.73382413 0 ;
	setAttr ".vl[0].vt[65]" -type "float3" 0 -0.73382413 0 ;
	setAttr ".vl[0].vt[66]" -type "float3" 0 -0.73382413 0 ;
	setAttr ".vl[0].vt[67]" -type "float3" 0 -0.73382413 0 ;
	setAttr ".vl[0].vt[68]" -type "float3" 0 -0.73382413 0 ;
	setAttr ".vl[0].vt[69]" -type "float3" 0 -0.73382413 0 ;
	setAttr ".vl[0].vt[70]" -type "float3" 0 -0.97072262 0 ;
	setAttr ".vl[0].vt[71]" -type "float3" 0 -0.97072262 0 ;
	setAttr ".vl[0].vt[72]" -type "float3" 0 -0.97072262 0 ;
	setAttr ".vl[0].vt[73]" -type "float3" 0 -0.97072262 0 ;
	setAttr ".vl[0].vt[74]" -type "float3" 0 -0.97072262 0 ;
	setAttr ".vl[0].vt[75]" -type "float3" 0 -0.97072262 0 ;
	setAttr ".vl[0].vt[76]" -type "float3" 0 -0.97072262 0 ;
	setAttr ".vl[0].vt[77]" -type "float3" 0 -0.97072262 0 ;
	setAttr ".vl[0].vt[78]" -type "float3" 0 -0.97072262 0 ;
	setAttr ".vl[0].vt[79]" -type "float3" 0 -0.97072262 0 ;
	setAttr ".vl[0].vt[80]" -type "float3" 0 -1.2272471 0 ;
	setAttr ".vl[0].vt[81]" -type "float3" 0 -1.2272471 0 ;
	setAttr ".vl[0].vt[82]" -type "float3" 0 -1.2272471 0 ;
	setAttr ".vl[0].vt[83]" -type "float3" 0 -1.2272471 0 ;
	setAttr ".vl[0].vt[84]" -type "float3" 0 -1.2272471 0 ;
	setAttr ".vl[0].vt[85]" -type "float3" 0 -1.2272471 0 ;
	setAttr ".vl[0].vt[86]" -type "float3" 0 -1.2272471 0 ;
	setAttr ".vl[0].vt[87]" -type "float3" 0 -1.2272471 0 ;
	setAttr ".vl[0].vt[88]" -type "float3" 0 -1.2272471 0 ;
	setAttr ".vl[0].vt[89]" -type "float3" 0 -1.2272471 0 ;
	setAttr ".vl[0].vt[90]" -type "float3" 0 -1.4970978 0 ;
	setAttr ".vl[0].vt[91]" -type "float3" 0 -1.4970978 0 ;
	setAttr ".vl[0].vt[92]" -type "float3" 0 -1.4970978 0 ;
	setAttr ".vl[0].vt[93]" -type "float3" 0 -1.4970978 0 ;
	setAttr ".vl[0].vt[94]" -type "float3" 0 -1.4970978 0 ;
	setAttr ".vl[0].vt[95]" -type "float3" 0 -1.4970978 0 ;
	setAttr ".vl[0].vt[96]" -type "float3" 0 -1.4970978 0 ;
	setAttr ".vl[0].vt[97]" -type "float3" 0 -1.4970978 0 ;
	setAttr ".vl[0].vt[98]" -type "float3" 0 -1.4970978 0 ;
	setAttr ".vl[0].vt[99]" -type "float3" 0 -1.4970978 0 ;
	setAttr ".vl[0].vt[100]" -type "float3" 0 -1.7880703 0 ;
	setAttr ".vl[0].vt[101]" -type "float3" 0 -1.7880703 0 ;
	setAttr ".vl[0].vt[102]" -type "float3" 0 -1.7880703 0 ;
	setAttr ".vl[0].vt[103]" -type "float3" 0 -1.7880703 0 ;
	setAttr ".vl[0].vt[104]" -type "float3" 0 -1.7880703 0 ;
	setAttr ".vl[0].vt[105]" -type "float3" 0 -1.7880703 0 ;
	setAttr ".vl[0].vt[106]" -type "float3" 0 -1.7880703 0 ;
	setAttr ".vl[0].vt[107]" -type "float3" 0 -1.7880703 0 ;
	setAttr ".vl[0].vt[108]" -type "float3" 0 -1.7880703 0 ;
	setAttr ".vl[0].vt[109]" -type "float3" 0 -1.7880703 0 ;
	setAttr ".vl[0].vt[110]" -type "float3" 0 -2.1110787 0 ;
	setAttr ".vl[0].vt[111]" -type "float3" 0 -2.1110787 0 ;
	setAttr ".vl[0].vt[112]" -type "float3" 0 -2.1110787 0 ;
	setAttr ".vl[0].vt[113]" -type "float3" 0 -2.1110787 0 ;
	setAttr ".vl[0].vt[114]" -type "float3" 0 -2.1110787 0 ;
	setAttr ".vl[0].vt[115]" -type "float3" 0 -2.1110787 0 ;
	setAttr ".vl[0].vt[116]" -type "float3" 0 -2.1110787 0 ;
	setAttr ".vl[0].vt[117]" -type "float3" 0 -2.1110787 0 ;
	setAttr ".vl[0].vt[118]" -type "float3" 0 -2.1110787 0 ;
	setAttr ".vl[0].vt[119]" -type "float3" 0 -2.1110787 0 ;
	setAttr ".vl[0].vt[120]" -type "float3" 0 -2.4550703 0 ;
	setAttr ".vl[0].vt[121]" -type "float3" 0 -2.4550703 0 ;
	setAttr ".vl[0].vt[122]" -type "float3" 0 -2.4550703 0 ;
	setAttr ".vl[0].vt[123]" -type "float3" 0 -2.4550703 0 ;
	setAttr ".vl[0].vt[124]" -type "float3" 0 -2.4550703 0 ;
	setAttr ".vl[0].vt[125]" -type "float3" 0 -2.4550703 0 ;
	setAttr ".vl[0].vt[126]" -type "float3" 0 -2.4550703 0 ;
	setAttr ".vl[0].vt[127]" -type "float3" 0 -2.4550703 0 ;
	setAttr ".vl[0].vt[128]" -type "float3" 0 -2.4550703 0 ;
	setAttr ".vl[0].vt[129]" -type "float3" 0 -2.4550703 0 ;
	setAttr ".vl[0].vt[130]" -type "float3" 0 -2.8098397 0 ;
	setAttr ".vl[0].vt[131]" -type "float3" 0 -2.8098397 0 ;
	setAttr ".vl[0].vt[132]" -type "float3" 0 -2.8098397 0 ;
	setAttr ".vl[0].vt[133]" -type "float3" 0 -2.8098397 0 ;
	setAttr ".vl[0].vt[134]" -type "float3" 0 -2.8098397 0 ;
	setAttr ".vl[0].vt[135]" -type "float3" 0 -2.8098397 0 ;
	setAttr ".vl[0].vt[136]" -type "float3" 0 -2.8098397 0 ;
	setAttr ".vl[0].vt[137]" -type "float3" 0 -2.8098397 0 ;
	setAttr ".vl[0].vt[138]" -type "float3" 0 -2.8098397 0 ;
	setAttr ".vl[0].vt[139]" -type "float3" 0 -2.8098397 0 ;
	setAttr ".vl[0].vt[140]" -type "float3" 0 -3.1656551 0 ;
	setAttr ".vl[0].vt[141]" -type "float3" 0 -3.1656551 0 ;
	setAttr ".vl[0].vt[142]" -type "float3" 0 -3.1656551 0 ;
	setAttr ".vl[0].vt[143]" -type "float3" 0 -3.1656551 0 ;
	setAttr ".vl[0].vt[144]" -type "float3" 0 -3.1656551 0 ;
	setAttr ".vl[0].vt[145]" -type "float3" 0 -3.1656551 0 ;
	setAttr ".vl[0].vt[146]" -type "float3" 0 -3.1656551 0 ;
	setAttr ".vl[0].vt[147]" -type "float3" 0 -3.1656551 0 ;
	setAttr ".vl[0].vt[148]" -type "float3" 0 -3.1656551 0 ;
	setAttr ".vl[0].vt[149]" -type "float3" 0 -3.1656551 0 ;
	setAttr ".vl[0].vt[150]" -type "float3" 0 -3.5185127 0 ;
	setAttr ".vl[0].vt[151]" -type "float3" 0 -3.5185127 0 ;
	setAttr ".vl[0].vt[152]" -type "float3" 0 -3.5185127 0 ;
	setAttr ".vl[0].vt[153]" -type "float3" 0 -3.5185127 0 ;
	setAttr ".vl[0].vt[154]" -type "float3" 0 -3.5185127 0 ;
	setAttr ".vl[0].vt[155]" -type "float3" 0 -3.5185127 0 ;
	setAttr ".vl[0].vt[156]" -type "float3" 0 -3.5185127 0 ;
	setAttr ".vl[0].vt[157]" -type "float3" 0 -3.5185127 0 ;
	setAttr ".vl[0].vt[158]" -type "float3" 0 -3.5185127 0 ;
	setAttr ".vl[0].vt[159]" -type "float3" 0 -3.5185127 0 ;
	setAttr ".vl[0].vt[160]" -type "float3" 0 -3.8836727 0 ;
	setAttr ".vl[0].vt[161]" -type "float3" 0 -3.8836727 0 ;
	setAttr ".vl[0].vt[162]" -type "float3" 0 -3.8836727 0 ;
	setAttr ".vl[0].vt[163]" -type "float3" 0 -3.8836727 0 ;
	setAttr ".vl[0].vt[164]" -type "float3" 0 -3.8836727 0 ;
	setAttr ".vl[0].vt[165]" -type "float3" 0 -3.8836727 0 ;
	setAttr ".vl[0].vt[166]" -type "float3" 0 -3.8836727 0 ;
	setAttr ".vl[0].vt[167]" -type "float3" 0 -3.8836727 0 ;
	setAttr ".vl[0].vt[168]" -type "float3" 0 -3.8836727 0 ;
	setAttr ".vl[0].vt[169]" -type "float3" 0 -3.8836727 0 ;
	setAttr ".vl[0].vt[170]" -type "float3" 0 -4.2776647 0 ;
	setAttr ".vl[0].vt[171]" -type "float3" 0 -4.2776647 0 ;
	setAttr ".vl[0].vt[172]" -type "float3" 0 -4.2776647 0 ;
	setAttr ".vl[0].vt[173]" -type "float3" 0 -4.2776647 0 ;
	setAttr ".vl[0].vt[174]" -type "float3" 0 -4.2776647 0 ;
	setAttr ".vl[0].vt[175]" -type "float3" 0 -4.2776647 0 ;
	setAttr ".vl[0].vt[176]" -type "float3" 0 -4.2776647 0 ;
	setAttr ".vl[0].vt[177]" -type "float3" 0 -4.2776647 0 ;
	setAttr ".vl[0].vt[178]" -type "float3" 0 -4.2776647 0 ;
	setAttr ".vl[0].vt[179]" -type "float3" 0 -4.2776647 0 ;
	setAttr ".vl[0].vt[180]" -type "float3" 0 -4.6832108 0 ;
	setAttr ".vl[0].vt[181]" -type "float3" 0 -4.6832108 0 ;
	setAttr ".vl[0].vt[182]" -type "float3" 0 -4.6832108 0 ;
	setAttr ".vl[0].vt[183]" -type "float3" 0 -4.6832108 0 ;
	setAttr ".vl[0].vt[184]" -type "float3" 0 -4.6832108 0 ;
	setAttr ".vl[0].vt[185]" -type "float3" 0 -4.6832108 0 ;
	setAttr ".vl[0].vt[186]" -type "float3" 0 -4.6832108 0 ;
	setAttr ".vl[0].vt[187]" -type "float3" 0 -4.6832108 0 ;
	setAttr ".vl[0].vt[188]" -type "float3" 0 -4.6832108 0 ;
	setAttr ".vl[0].vt[189]" -type "float3" 0 -4.6832108 0 ;
	setAttr ".vl[0].vt[190]" -type "float3" 0 -5.0656724 0 ;
	setAttr ".vl[0].vt[191]" -type "float3" 0 -5.0656724 0 ;
	setAttr ".vl[0].vt[192]" -type "float3" 0 -5.0656724 0 ;
	setAttr ".vl[0].vt[193]" -type "float3" 0 -5.0656724 0 ;
	setAttr ".vl[0].vt[194]" -type "float3" 0 -5.0656724 0 ;
	setAttr ".vl[0].vt[195]" -type "float3" 0 -5.0656724 0 ;
	setAttr ".vl[0].vt[196]" -type "float3" 0 -5.0656724 0 ;
	setAttr ".vl[0].vt[197]" -type "float3" 0 -5.0656724 0 ;
	setAttr ".vl[0].vt[198]" -type "float3" 0 -5.0656724 0 ;
	setAttr ".vl[0].vt[199]" -type "float3" 0 -5.0656724 0 ;
	setAttr ".vl[0].vt[200]" -type "float3" 0 -5.4936013 0 ;
createNode objectSet -n "tweakSet1";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId2";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster1";
	setAttr -s 201 ".wl";
	setAttr -s 2 ".wl[0].w[0:1]"  0.9065350666642189 0.093464933335781097;
	setAttr -s 2 ".wl[1].w[0:1]"  0.95280421897768974 0.047195781022310257;
	setAttr ".wl[2].w[0]"  1;
	setAttr ".wl[3].w[0]"  1;
	setAttr ".wl[4].w[0]"  1;
	setAttr ".wl[5].w[0]"  1;
	setAttr ".wl[6].w[0]"  1;
	setAttr ".wl[7].w[0]"  1;
	setAttr ".wl[8].w[0]"  1;
	setAttr -s 2 ".wl[9].w[0:1]"  0.91222451075871869 0.08777548924128134;
	setAttr -s 2 ".wl[10].w[0:1]"  0.73074286849379599 0.26925713150620406;
	setAttr -s 2 ".wl[11].w[0:1]"  0.79285865678961021 0.20714134321038977;
	setAttr ".wl[12].w[0]"  1;
	setAttr ".wl[13].w[0]"  1;
	setAttr ".wl[14].w[0]"  1;
	setAttr -s 2 ".wl[15].w[0:1]"  0.89567521959543228 0.10432478040456772;
	setAttr ".wl[16].w[0]"  1;
	setAttr -s 2 ".wl[17].w[0:1]"  0.77277402055901112 0.22722597944098896;
	setAttr -s 2 ".wl[18].w[0:1]"  0.7007253449854719 0.29927465501452816;
	setAttr -s 2 ".wl[19].w[0:1]"  0.68994964143554749 0.31005035856445246;
	setAttr -s 2 ".wl[20].w[0:1]"  0.47949868440628052 0.52050131559371948;
	setAttr -s 2 ".wl[21].w[0:1]"  0.48598712682723999 0.51401287317276001;
	setAttr -s 2 ".wl[22].w[0:1]"  0.54843565821647644 0.45156434178352356;
	setAttr -s 2 ".wl[23].w[0:1]"  0.5448092520236969 0.4551907479763031;
	setAttr -s 2 ".wl[24].w[0:1]"  0.47451585531234741 0.52548414468765259;
	setAttr -s 2 ".wl[25].w[0:1]"  0.47837609052658081 0.52162390947341919;
	setAttr -s 2 ".wl[26].w[0:1]"  0.52448910474777222 0.47551089525222778;
	setAttr -s 2 ".wl[27].w[0:1]"  0.46746432781219482 0.53253567218780518;
	setAttr -s 2 ".wl[28].w[0:1]"  0.45964205265045166 0.54035794734954834;
	setAttr -s 2 ".wl[29].w[0:1]"  0.4640812873840332 0.5359187126159668;
	setAttr -s 2 ".wl[30].w[0:1]"  0.24215209484100342 0.75784790515899658;
	setAttr -s 2 ".wl[31].w[0:1]"  0.2354925274848938 0.7645074725151062;
	setAttr -s 2 ".wl[32].w[0:1]"  0.26658374071121216 0.73341625928878784;
	setAttr -s 2 ".wl[33].w[0:1]"  0.25294321775436401 0.74705678224563599;
	setAttr -s 2 ".wl[34].w[0:1]"  0.20037215948104858 0.79962784051895142;
	setAttr -s 2 ".wl[35].w[0:1]"  0.19357496500015259 0.80642503499984741;
	setAttr -s 2 ".wl[36].w[0:1]"  0.20897454023361206 0.79102545976638794;
	setAttr -s 2 ".wl[37].w[0:1]"  0.21744966506958008 0.78255033493041992;
	setAttr -s 2 ".wl[38].w[0:1]"  0.20598971843719482 0.79401028156280518;
	setAttr -s 2 ".wl[39].w[0:1]"  0.23257559537887573 0.76742440462112427;
	setAttr -s 2 ".wl[40].w[0:1]"  0.10210120677947998 0.89789879322052002;
	setAttr -s 2 ".wl[41].w[0:1]"  0.10749912261962891 0.89250087738037109;
	setAttr -s 2 ".wl[42].w[0:1]"  0.088934838771820068 0.91106516122817993;
	setAttr -s 3 ".wl[43].w[0:2]"  0.042521465343806061 0.92325633764266968 
		0.034222197013524261;
	setAttr -s 3 ".wl[44].w[0:2]"  0.064041463443624033 0.88428932428359985 
		0.05166921227277612;
	setAttr -s 3 ".wl[45].w[0:2]"  0.042058982613061568 0.92392390966415405 
		0.034017107722784379;
	setAttr -s 3 ".wl[46].w[0:2]"  0.013516483493740716 0.97553485631942749 
		0.010948660186831794;
	setAttr -s 2 ".wl[47].w[0:1]"  0.048566997051239014 0.95143300294876099;
	setAttr -s 2 ".wl[48].w[0:1]"  0.071418702602386475 0.92858129739761353;
	setAttr -s 2 ".wl[49].w[0:1]"  0.10614043474197388 0.89385956525802612;
	setAttr -s 3 ".wl[50].w[0:2]"  0.033053048998245238 0.9457158212362442 
		0.021231129765510559;
	setAttr -s 3 ".wl[51].w[0:2]"  0.012285162624659703 0.9733999530554388 
		0.014314884319901466;
	setAttr -s 3 ".wl[52].w[0:2]"  0.017806630188404358 0.96076732873916626 
		0.021426041072429378;
	setAttr -s 3 ".wl[53].w[0:2]"  0.04222305113804304 0.88176391278970279 
		0.076013036072254181;
	setAttr -s 3 ".wl[54].w[0:2]"  0.046201417674620604 0.84850971169273093 
		0.10528887063264847;
	setAttr -s 3 ".wl[55].w[0:2]"  0.044613014515623314 0.86353586280705052 
		0.091851122677326202;
	setAttr -s 3 ".wl[56].w[0:2]"  0.025201127173884567 0.94427037239074707 
		0.03052850043536836;
	setAttr -s 3 ".wl[57].w[0:2]"  0.014549349917403276 0.96782565116882324 
		0.01762499891377348;
	setAttr -s 3 ".wl[58].w[0:2]"  0.046440186442295868 0.91614665230973547 
		0.037413161247968674;
	setAttr -s 3 ".wl[59].w[0:2]"  0.052147338074794991 0.9185818509187782 
		0.029270811006426811;
	setAttr -s 3 ".wl[60].w[0:2]"  0.033615048446979892 0.8638542858573528 
		0.10253066569566727;
	setAttr -s 3 ".wl[61].w[0:2]"  0.019529381227360063 0.92877784984125111 
		0.051692768931388855;
	setAttr -s 3 ".wl[62].w[0:2]"  0.043696817949312357 0.84799889753769098 
		0.10830428451299667;
	setAttr -s 3 ".wl[63].w[0:2]"  0.054362519545237062 0.77303530378850316 
		0.17260217666625977;
	setAttr -s 3 ".wl[64].w[0:2]"  0.056613253296133016 0.73795997923541456 
		0.20542676746845245;
	setAttr -s 3 ".wl[65].w[0:2]"  0.069430272089654174 0.72805521358329661 
		0.20251451432704926;
	setAttr -s 3 ".wl[66].w[0:2]"  0.049653373884450958 0.77193482942651559 
		0.17841179668903351;
	setAttr -s 3 ".wl[67].w[0:2]"  0.044049592326208031 0.79823604076095023 
		0.1577143669128418;
	setAttr -s 3 ".wl[68].w[0:2]"  0.042894971973816226 0.80775046586283084 
		0.14935456216335297;
	setAttr -s 3 ".wl[69].w[0:2]"  0.1063729120790331 0.75103166267282084 
		0.14259542524814606;
	setAttr -s 2 ".wl[70].w[1:2]"  0.74036777019500732 0.25963222980499268;
	setAttr -s 3 ".wl[71].w[0:2]"  0.0027731070981732615 0.80166268248694028 
		0.19556421041488647;
	setAttr -s 3 ".wl[72].w[0:2]"  0.052318598438730886 0.72822505671835835 
		0.21945634484291077;
	setAttr -s 2 ".wl[73].w[1:2]"  0.69494977593421936 0.30505022406578064;
	setAttr -s 2 ".wl[74].w[1:2]"  0.65375357866287231 0.34624642133712769;
	setAttr -s 3 ".wl[75].w[0:2]"  0.09658262543542763 0.54654722995893579 
		0.3568701446056366;
	setAttr -s 3 ".wl[76].w[0:2]"  0.091501830618246877 0.54636365314258684 
		0.36213451623916626;
	setAttr -s 3 ".wl[77].w[0:2]"  0.089378344028794465 0.54820067980924869 
		0.36242097616195679;
	setAttr -s 2 ".wl[78].w[1:2]"  0.64869040250778198 0.35130959749221802;
	setAttr -s 2 ".wl[79].w[1:2]"  0.64892315864562988 0.35107684135437012;
	setAttr -s 2 ".wl[80].w[1:2]"  0.43624725937843323 0.56375274062156677;
	setAttr -s 2 ".wl[81].w[1:2]"  0.54527145624160767 0.45472854375839233;
	setAttr -s 2 ".wl[82].w[1:2]"  0.54269111156463623 0.45730888843536377;
	setAttr -s 2 ".wl[83].w[1:2]"  0.51593297719955444 0.48406702280044556;
	setAttr -s 2 ".wl[84].w[1:2]"  0.47354894876480103 0.52645105123519897;
	setAttr -s 2 ".wl[85].w[1:2]"  0.4688991904258728 0.5311008095741272;
	setAttr -s 2 ".wl[86].w[1:2]"  0.4549681544303894 0.5450318455696106;
	setAttr -s 2 ".wl[87].w[1:2]"  0.43157470226287842 0.56842529773712158;
	setAttr -s 2 ".wl[88].w[1:2]"  0.3995133638381958 0.6004866361618042;
	setAttr -s 2 ".wl[89].w[1:2]"  0.38764727115631104 0.61235272884368896;
	setAttr -s 2 ".wl[90].w[1:2]"  0.2213752418756485 0.7786247581243515;
	setAttr -s 2 ".wl[91].w[1:2]"  0.31318247318267822 0.68681752681732178;
	setAttr -s 2 ".wl[92].w[1:2]"  0.348926842212677 0.651073157787323;
	setAttr -s 2 ".wl[93].w[1:2]"  0.35298681259155273 0.64701318740844727;
	setAttr -s 2 ".wl[94].w[1:2]"  0.33709263801574707 0.66290736198425293;
	setAttr -s 2 ".wl[95].w[1:2]"  0.3203432559967041 0.6796567440032959;
	setAttr -s 2 ".wl[96].w[1:2]"  0.29330247640609741 0.70669752359390259;
	setAttr -s 2 ".wl[97].w[1:2]"  0.24439167976379395 0.75560832023620605;
	setAttr -s 2 ".wl[98].w[1:2]"  0.1409800797700882 0.8590199202299118;
	setAttr -s 2 ".wl[99].w[1:2]"  0.19526229798793793 0.80473770201206207;
	setAttr -s 2 ".wl[100].w[1:2]"  0.099578998982906342 0.90042100101709366;
	setAttr -s 2 ".wl[101].w[1:2]"  0.16324383020401001 0.83675616979598999;
	setAttr -s 2 ".wl[102].w[1:2]"  0.21335792541503906 0.78664207458496094;
	setAttr -s 2 ".wl[103].w[1:2]"  0.23269754648208618 0.76730245351791382;
	setAttr -s 2 ".wl[104].w[1:2]"  0.22658663988113403 0.77341336011886597;
	setAttr -s 2 ".wl[105].w[1:2]"  0.20068687200546265 0.79931312799453735;
	setAttr -s 2 ".wl[106].w[1:2]"  0.1759454607963562 0.8240545392036438;
	setAttr -s 2 ".wl[107].w[1:2]"  0.12568020820617676 0.87431979179382324;
	setAttr -s 2 ".wl[108].w[1:2]"  0.04094402864575386 0.95905597135424614;
	setAttr -s 2 ".wl[109].w[1:2]"  0.070205934345722198 0.9297940656542778;
	setAttr -s 2 ".wl[110].w[1:2]"  0.038139261305332184 0.96186073869466782;
	setAttr -s 2 ".wl[111].w[1:2]"  0.065094619989395142 0.93490538001060486;
	setAttr -s 2 ".wl[112].w[1:2]"  0.12003839015960693 0.87996160984039307;
	setAttr -s 2 ".wl[113].w[1:2]"  0.14552509784698486 0.85447490215301514;
	setAttr -s 2 ".wl[114].w[1:2]"  0.14834797382354736 0.85165202617645264;
	setAttr -s 2 ".wl[115].w[1:2]"  0.12875258922576904 0.87124741077423096;
	setAttr -s 2 ".wl[116].w[1:2]"  0.10223996639251709 0.89776003360748291;
	setAttr -s 2 ".wl[117].w[1:2]"  0.063863158226013184 0.93613684177398682;
	setAttr -s 2 ".wl[118].w[1:2]"  0.016071617603302002 0.983928382396698;
	setAttr -s 2 ".wl[119].w[1:2]"  0.019146416336297989 0.98085358366370201;
	setAttr -s 2 ".wl[120].w[1:2]"  0.016599588096141815 0.98340041190385818;
	setAttr -s 2 ".wl[121].w[1:2]"  0.033358769440716939 0.96664123055928308;
	setAttr -s 2 ".wl[122].w[1:2]"  0.067141294479370117 0.93285870552062988;
	setAttr -s 2 ".wl[123].w[1:2]"  0.092267274856567383 0.90773272514343262;
	setAttr -s 2 ".wl[124].w[1:2]"  0.098733365535736084 0.90126663446426392;
	setAttr -s 2 ".wl[125].w[1:2]"  0.087590217590332031 0.91240978240966797;
	setAttr -s 2 ".wl[126].w[1:2]"  0.066379308700561523 0.93362069129943848;
	setAttr -s 2 ".wl[127].w[1:2]"  0.040503263473510742 0.95949673652648926;
	setAttr -s 2 ".wl[128].w[1:2]"  0.016786813735961914 0.98321318626403809;
	setAttr -s 2 ".wl[129].w[1:2]"  0.0086030922830104828 0.99139690771698952;
	setAttr -s 2 ".wl[130].w[1:2]"  0.022745806723833084 0.97725419327616692;
	setAttr -s 2 ".wl[131].w[1:2]"  0.033209331333637238 0.96679066866636276;
	setAttr -s 2 ".wl[132].w[1:2]"  0.046263460069894791 0.95373653993010521;
	setAttr -s 2 ".wl[133].w[1:2]"  0.064528167247772217 0.93547183275222778;
	setAttr -s 2 ".wl[134].w[1:2]"  0.071754932403564453 0.92824506759643555;
	setAttr -s 2 ".wl[135].w[1:2]"  0.067671775817871094 0.93232822418212891;
	setAttr -s 2 ".wl[136].w[1:2]"  0.052746832370758057 0.94725316762924194;
	setAttr -s 2 ".wl[137].w[1:2]"  0.036857187747955322 0.96314281225204468;
	setAttr -s 2 ".wl[138].w[1:2]"  0.026121174916625023 0.97387882508337498;
	setAttr -s 2 ".wl[139].w[1:2]"  0.019627280533313751 0.98037271946668625;
	setAttr -s 2 ".wl[140].w[1:2]"  0.03043246828019619 0.96956753171980381;
	setAttr -s 2 ".wl[141].w[1:2]"  0.037251655012369156 0.96274834498763084;
	setAttr -s 2 ".wl[142].w[1:2]"  0.047650150954723358 0.95234984904527664;
	setAttr -s 2 ".wl[143].w[1:2]"  0.058684747666120529 0.94131525233387947;
	setAttr -s 2 ".wl[144].w[1:2]"  0.065033867955207825 0.93496613204479218;
	setAttr -s 2 ".wl[145].w[1:2]"  0.063719265162944794 0.93628073483705521;
	setAttr -s 2 ".wl[146].w[1:2]"  0.0557415671646595 0.9442584328353405;
	setAttr -s 2 ".wl[147].w[1:2]"  0.045262064784765244 0.95473793521523476;
	setAttr -s 2 ".wl[148].w[1:2]"  0.036709446460008621 0.96329055353999138;
	setAttr -s 2 ".wl[149].w[1:2]"  0.031158251687884331 0.96884174831211567;
	setAttr -s 2 ".wl[150].w[1:2]"  0.03796343132853508 0.96203656867146492;
	setAttr -s 2 ".wl[151].w[1:2]"  0.042886592447757721 0.95711340755224228;
	setAttr -s 2 ".wl[152].w[1:2]"  0.050849791616201401 0.9491502083837986;
	setAttr -s 2 ".wl[153].w[1:2]"  0.059352710843086243 0.94064728915691376;
	setAttr -s 2 ".wl[154].w[1:2]"  0.064347907900810242 0.93565209209918976;
	setAttr -s 3 ".wl[155].w[1:3]"  0.06359051913022995 0.78916136113223512 
		0.14724811973753493;
	setAttr -s 2 ".wl[156].w[1:2]"  0.057374276220798492 0.94262572377920151;
	setAttr -s 2 ".wl[157].w[1:2]"  0.049608204513788223 0.95039179548621178;
	setAttr -s 2 ".wl[158].w[1:2]"  0.042918846011161804 0.9570811539888382;
	setAttr -s 2 ".wl[159].w[1:2]"  0.038589615374803543 0.96141038462519646;
	setAttr -s 3 ".wl[160].w[1:3]"  0.041660059243440628 0.84077908166247906 
		0.11756085909408026;
	setAttr -s 3 ".wl[161].w[1:3]"  0.045912753790616989 0.82454787894368031 
		0.12953936726570267;
	setAttr -s 2 ".wl[162].w[1:2]"  0.052257228642702103 0.9477427713572979;
	setAttr -s 2 ".wl[163].w[1:2]"  0.058998461812734604 0.9410015381872654;
	setAttr -s 3 ".wl[164].w[1:3]"  0.06133926659822464 0.68029590506409376 
		0.2583648283376816;
	setAttr -s 3 ".wl[165].w[1:3]"  0.060635637491941452 0.65506667188375889 
		0.28429769062429966;
	setAttr -s 3 ".wl[166].w[1:3]"  0.055923882871866226 0.63869673766870616 
		0.30537937945942756;
	setAttr -s 3 ".wl[167].w[1:3]"  0.049791246652603149 0.72184898858271573 
		0.22835976476468109;
	setAttr -s 3 ".wl[168].w[1:3]"  0.044166099280118942 0.7722547583370345 
		0.18357914238284659;
	setAttr -s 3 ".wl[169].w[1:3]"  0.041803423315286636 0.82931079441574018 
		0.12888578226897315;
	setAttr -s 3 ".wl[170].w[1:3]"  0.040387272834777832 0.7724603243453223 
		0.18715240281989984;
	setAttr -s 3 ".wl[171].w[1:3]"  0.043041113764047623 0.74267533738117431 
		0.2142835488547781;
	setAttr -s 3 ".wl[172].w[1:3]"  0.046833064407110214 0.67443259299594049 
		0.27873434259694935;
	setAttr -s 3 ".wl[173].w[1:3]"  0.057336603899585191 0.56256108949602945 
		0.38010230660438538;
	setAttr -s 3 ".wl[174].w[1:3]"  0.051987583528295939 0.51073864590094908 
		0.437273770570755;
	setAttr -s 3 ".wl[175].w[1:3]"  0.051829423755407333 0.48584613480421857 
		0.46232444144037405;
	setAttr -s 3 ".wl[176].w[1:3]"  0.049520280212163925 0.48207106291960916 
		0.46840865686822691;
	setAttr -s 3 ".wl[177].w[1:3]"  0.045079480856657028 0.51044232691213931 
		0.44447819223120366;
	setAttr -s 3 ".wl[178].w[1:3]"  0.041174933314323425 0.61532364375806103 
		0.3435014229276156;
	setAttr -s 3 ".wl[179].w[1:3]"  0.039888475090265274 0.71447520012136378 
		0.24563632478837091;
	setAttr -s 3 ".wl[180].w[1:3]"  0.029118873178958893 0.56233881018890308 
		0.40854231663213808;
	setAttr -s 3 ".wl[181].w[1:3]"  0.032650534063577652 0.5122787273690742 
		0.45507073856734814;
	setAttr -s 3 ".wl[182].w[1:3]"  0.028211485682411244 0.44017790621669767 
		0.53161060810089111;
	setAttr -s 3 ".wl[183].w[1:3]"  0.025159915985736483 0.39288691609891929 
		0.58195316791534424;
	setAttr -s 3 ".wl[184].w[1:3]"  0.023291396916400697 0.36419303435038397 
		0.61251556873321533;
	setAttr -s 3 ".wl[185].w[1:3]"  0.022499267784344468 0.35227685716987917 
		0.62522387504577637;
	setAttr -s 3 ".wl[186].w[1:3]"  0.024492012336850166 0.34705506488276477 
		0.628452922780385;
	setAttr -s 3 ".wl[187].w[1:3]"  0.025552323088049889 0.34580441573303122 
		0.62864326117891889;
	setAttr -s 3 ".wl[188].w[1:3]"  0.02781420573592186 0.39718574012191404 
		0.57500005414216415;
	setAttr -s 3 ".wl[189].w[1:3]"  0.029443874955177307 0.48945999779005051 
		0.48109612725477219;
	setAttr -s 3 ".wl[190].w[1:3]"  0.0094565153907945707 0.31717121593244763 
		0.67337226867675781;
	setAttr -s 3 ".wl[191].w[1:3]"  0.0082764326491545168 0.27741524587486283 
		0.71430832147598267;
	setAttr -s 3 ".wl[192].w[1:3]"  0.0066798864872291905 0.22390109905888286 
		0.76941901445388794;
	setAttr -s 3 ".wl[193].w[1:3]"  0.0077605425564880245 0.26028833992794803 
		0.73195111751556396;
	setAttr -s 3 ".wl[194].w[1:3]"  0.007714190501762334 0.2589999651722184 
		0.73328584432601929;
	setAttr -s 3 ".wl[195].w[1:3]"  0.0074953457592780793 0.25191205532562183 
		0.7405925989151001;
	setAttr -s 3 ".wl[196].w[1:3]"  0.0066228153576041336 0.22272930122717369 
		0.77064788341522217;
	setAttr -s 3 ".wl[197].w[1:3]"  0.0064878110590461269 0.2181890363988396 
		0.77532315254211426;
	setAttr -s 3 ".wl[198].w[1:3]"  0.007574863762420822 0.2545846029810484 
		0.73784053325653076;
	setAttr -s 3 ".wl[199].w[1:3]"  0.008827782660615251 0.29638829132925293 
		0.69478392601013184;
	setAttr -s 2 ".wl[200].w[2:3]"  0.20150768756866455 0.79849231243133545;
	setAttr -s 4 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 0.007165049268386783 0.0048754599062838392 1;
	setAttr ".pm[1]" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 -0.90930740001994259 0.0048754599062838392 1;
	setAttr ".pm[2]" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -2.9873315002025819e-021 -2.7563518747394991 -0.0021743281651649413 1;
	setAttr ".pm[3]" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -3.8662609262496784e-016 -4.5063986778259268 -2.2161092405603711e-016 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 5 0 1;
	setAttr -s 4 ".ma";
	setAttr -s 4 ".dpf[0:3]"  2 2 2 2;
	setAttr -s 4 ".lw";
	setAttr -s 4 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 3;
	setAttr ".bm" 0;
	setAttr ".ucm" yes;
	setAttr -s 4 ".ifcl";
	setAttr -s 4 ".ifcl";
createNode objectSet -n "skinCluster1Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster1GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster1GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode dagPose -n "bindPose1";
	setAttr -s 4 ".wm";
	setAttr -s 4 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 -0.007165049268386783
		 -0.0048754599062838392 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0.91647244928832938
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 0 0 0 0 2.9873315002025819e-021
		 1.8470444747195567 0.0070497880714487805 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1
		 1 1 yes;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 0 0 0 0 3.8662310529346764e-016
		 1.7500468030864278 -0.0021743281651647197 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
		0 0 0 1 1 1 1 yes;
	setAttr -s 4 ".m";
	setAttr -s 4 ".p";
	setAttr ".bp" yes;
createNode animCurveTA -n "joint1_rotateX";
	setAttr ".tan" 16;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "joint1_rotateY";
	setAttr ".tan" 16;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "joint1_rotateZ";
	setAttr ".tan" 16;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "joint3_rotateX";
	setAttr ".tan" 16;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "joint2_rotateX";
	setAttr ".tan" 16;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "joint3_rotateY";
	setAttr ".tan" 16;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "joint2_rotateY";
	setAttr ".tan" 16;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "joint3_rotateZ";
	setAttr ".tan" 16;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 2 117.29117078027811;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "joint2_rotateZ";
	setAttr ".tan" 16;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 2 -80.903170968612045;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode script -n "uiConfigurationScriptNode";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n"
		+ "                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n"
		+ "                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n"
		+ "            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n"
		+ "                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n"
		+ "                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n"
		+ "            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n"
		+ "                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n"
		+ "                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n"
		+ "                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n"
		+ "            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n"
		+ "            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n"
		+ "        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n"
		+ "                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n"
		+ "                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n"
		+ "                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n"
		+ "            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n"
		+ "            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n"
		+ "            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n"
		+ "                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 1\n            -showConnected 1\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n"
		+ "            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n"
		+ "            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n"
		+ "                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n"
		+ "                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n"
		+ "                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n"
		+ "                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n"
		+ "                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n"
		+ "                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n"
		+ "                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n"
		+ "            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Texture Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"Stereo\" -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels `;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n"
		+ "                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n"
		+ "                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n"
		+ "                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n"
		+ "                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n"
		+ "                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n"
		+ "\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 1 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode displayLayer -n "skeleton";
	setAttr ".c" 6;
	setAttr ".do" 1;
createNode displayLayer -n "mesh";
	setAttr ".do" 2;
createNode polyTweakUV -n "polyTweakUV1";
	setAttr ".uopa" yes;
	setAttr -s 231 ".uvtk[0:230]" -type "float2" 0.23467198 -0.027657915 0.26658738
		 -0.0044699758 0.30603707 -0.0044699758 0.33795249 -0.027657907 0.35014307 -0.06517677
		 0.33795267 -0.10269561 0.30603707 -0.12588355 0.26658738 -0.12588358 0.23467198 -0.10269564
		 0.2224814 -0.06517677 -0.24045298 -0.060459144 -0.20672038 -0.095098741 -0.17075759
		 -0.12489209 -0.13144468 -0.14857876 -0.088800713 -0.164067 -0.044181012 -0.16949874
		 0.00045247003 -0.1641531 0.043131836 -0.14872578 0.082501903 -0.12505507 0.11854443
		 -0.095223732 0.15236631 -0.060494371 -0.22424313 -0.020775704 -0.19197789 -0.054092314
		 -0.15808216 -0.082200907 -0.12192611 -0.1039033 -0.08365415 -0.11778193 -0.044151805
		 -0.12260022 -0.0046480782 -0.11784907 0.03363467 -0.10401315 0.069820955 -0.082314692
		 0.10376474 -0.054159429 0.13609394 -0.020751713 -0.20989949 0.013862863 -0.17923903
		 -0.018800734 -0.14753821 -0.045937534 -0.11426492 -0.06646467 -0.079590365 -0.079374604
		 -0.044128589 -0.083820827 -0.0086747296 -0.079437159 0.025987394 -0.066567846 0.059261158
		 -0.046038058 0.090976864 -0.018836914 0.12167761 0.01394017 -0.19674331 0.043392614
		 -0.1679363 0.010844007 -0.13848063 -0.015826641 -0.10786386 -0.035698585 -0.076244801
		 -0.048037373 -0.044104926 -0.052261077 -0.011981539 -0.048111282 0.019596405 -0.035824709
		 0.050176755 -0.01594889 0.079606548 0.010811463 0.10842958 0.043518618 -0.18425488
		 0.067820735 -0.15763336 0.035049453 -0.13047211 0.0085514951 -0.10234608 -0.010952285
		 -0.073430479 -0.022965362 -0.044099025 -0.027086904 -0.014798395 -0.023074439 0.014074216
		 -0.011146953 0.042128749 0.0083701182 0.069209352 0.034987941 0.095788091 0.067977734
		 -0.17208898 0.0871998 -0.14801419 0.054176524 -0.12321131 0.02775671 -0.097423688
		 0.0084762145 -0.070942223 -0.0033346305 -0.044096582 -0.0073888805 -0.017262213 -0.0035056362
		 0.0091630891 0.0081972647 0.034836777 0.027497072 0.059499994 0.054060355 0.083442941
		 0.0873565 -0.16019282 0.10178285 -0.13888299 0.06875021 -0.11646782 0.04245688 -0.092971459
		 0.023374934 -0.068724476 0.011725143 -0.044112854 0.0077185798 -0.01951056 0.011516944
		 0.0047005508 0.023042757 0.028098293 0.042136565 0.050337568 0.068581767 0.071426108
		 0.10193121 -0.14854178 0.1121152 -0.13006371 0.07940232 -0.11006771 0.053401366 -0.088813677
		 0.034574404 -0.066699527 0.023100156 -0.044150345 0.019152358 -0.021596365 0.022885639
		 0.00053216144 0.034234479 0.021742947 0.053070143 0.041568823 0.079237871 0.059737518
		 0.11228602 -0.13694188 0.11895417 -0.12135209 0.086866789 -0.103855 0.061374202 -0.084823683
		 0.042922154 -0.064792477 0.031679112 -0.044199459 0.027811665 -0.023593836 0.031479258
		 -0.0034972318 0.042612627 0.015553186 0.061095968 0.032944925 0.086777441 0.048276082
		 0.11924022 -0.12504587 0.12317073 -0.11242332 0.091942064 -0.097516045 0.067189522
		 -0.080779448 0.049260572 -0.062829755 0.038328484 -0.044232748 0.034575 -0.025605313
		 0.038156584 -0.0075797923 0.049008325 0.0092476681 0.067008086 0.024034932 0.091909103
		 0.036426969 0.12345152 -0.11372493 0.12366008 -0.10400094 0.093646757 -0.091561243
		 0.069892831 -0.076988012 0.052650288 -0.06098678 0.042127505 -0.044257186 0.038523808
		 -0.027483426 0.041984096 -0.011411481 0.052453414 0.0032472108 0.069757171 0.015597472
		 0.093636684 0.025100902 0.12385332 -0.10445844 0.11917394 -0.097204253 0.090816908
		 -0.086779639 0.068294711 -0.0739519 0.051889077 -0.059506111 0.041862503 -0.044263534
		 0.038438812 -0.028986476 0.041751578 -0.014489584 0.051738337 -0.0015976913 0.068182774
		 0.008769758 0.09079998 0.015845785 0.11926406 -0.096513018 0.11131173 -0.091311201
		 0.085002534 -0.082596645 0.06391222 -0.071297348 0.048499778 -0.058205985 0.039052382
		 -0.044263206 0.0358385 -0.03029152 0.038975909 -0.017180197 0.048383906 -0.0058187135
		 0.063824549 0.0028522704 0.084949903 0.0079335645 0.11129814 -0.089027867 0.10139667
		 -0.085562274 0.077457659 -0.078456789 0.058039442 -0.068583481 0.043738976 -0.056888096
		 0.034985021 -0.04425206 0.032015461 -0.031605624 0.034941211 -0.019872896 0.043673709
		 -0.0099945553 0.057953373 -0.0029065497 0.077355556 0.00049502775 0.10127752 -0.080978379
		 0.09049993 -0.07909815 0.069208451 -0.073662341 0.051725343 -0.065427773 0.038776651
		 -0.055304997 0.030825755 -0.044229023 0.028140265 -0.033147298 0.030811807 -0.023014061
		 0.038746193 -0.014782839 0.051664725 -0.0093516 0.069081135 -0.0074892528 0.090299062
		 -0.071801335 0.078941695 -0.071471751 0.060464874 -0.067864113 0.045296803 -0.061541669
		 0.033983245 -0.053353094 0.027010877 -0.04417666 0.02465238 -0.035060726 0.027027924
		 -0.02684211 0.034006372 -0.020532541 0.045307532 -0.01691889 0.060418025 -0.016632967
		 0.078598909 -0.063170694 0.064706869 -0.06430205 0.049581245 -0.062240236 0.036971107
		 -0.057693027 0.027513523 -0.051336281 0.021637097 -0.044153236 0.019657269 -0.036984526
		 0.021660224 -0.030744515 0.027564246 -0.026183419 0.03697522 -0.024091236 0.049537733
		 -0.025045447 0.064621158 -0.057758383 0.046623066 -0.059382521 0.034992471 -0.058187746
		 0.025093693 -0.054791652 0.017526165 -0.049866699 0.012784496 -0.044147871 0.011170939
		 -0.038446628 0.012791827 -0.033569179 0.01755847 -0.030171178 0.025097031 -0.028944604
		 0.034996048 -0.030415349 0.046670809 -0.053780101 0.026557405 -0.055122189 0.018560067
		 -0.05451142 0.01149036 -0.052140884 0.0059682317 -0.048496686 0.002441067 -0.044179223
		 0.0012183932 -0.039857231 0.0024062579 -0.036187015 0.0058820434 -0.033779167 0.011462763
		 -0.033131979 0.018553451 -0.034478746 0.026589472 -0.04642307 0.0072810818 -0.047941379
		 0.0031880324 -0.04817047 -0.00049613439 -0.047432654 -0.0034193883 -0.045943968 -0.0052957423
		 -0.044070207 -0.0059366729 -0.042202912 -0.0052875169 -0.040759407 -0.0034133682
		 -0.040110849 -0.00046037161 -0.040352248 0.0032294577 -0.041751824 0.0073883701 -0.044119082
		 -0.018453119;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 2 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 18 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surfaces" "Particles" "Fluids" "Image Planes" "UI:" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 18 0 1 1 1 1 1
		 1 0 0 0 0 0 0 0 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "mesh.di" "m_tentacle.do";
connectAttr "groupId2.id" "m_tentacleShape.iog.og[5].gid";
connectAttr "tweakSet1.mwc" "m_tentacleShape.iog.og[5].gco";
connectAttr "skinCluster1GroupId.id" "m_tentacleShape.iog.og[6].gid";
connectAttr "skinCluster1Set.mwc" "m_tentacleShape.iog.og[6].gco";
connectAttr "polyTweakUV1.out" "m_tentacleShape.i";
connectAttr "tweak1.vl[0].vt[0]" "m_tentacleShape.twl";
connectAttr "polyTweakUV1.uvtk[0]" "m_tentacleShape.uvst[0].uvtw";
connectAttr "polyCone1.out" "m_tentacleShape1Orig.i";
connectAttr "joint1_rotateX.o" "joint1.rx";
connectAttr "joint1_rotateY.o" "joint1.ry";
connectAttr "joint1_rotateZ.o" "joint1.rz";
connectAttr "skeleton.di" "joint1.do";
connectAttr "joint1.s" "joint2.is";
connectAttr "joint2_rotateX.o" "joint2.rx";
connectAttr "joint2_rotateY.o" "joint2.ry";
connectAttr "joint2_rotateZ.o" "joint2.rz";
connectAttr "skeleton.di" "joint2.do";
connectAttr "joint2.s" "joint3.is";
connectAttr "joint3_rotateX.o" "joint3.rx";
connectAttr "joint3_rotateY.o" "joint3.ry";
connectAttr "joint3_rotateZ.o" "joint3.rz";
connectAttr "skeleton.di" "joint3.do";
connectAttr "joint3.s" "joint4.is";
connectAttr "skeleton.di" "joint4.do";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "groupParts2.og" "tweak1.ip[0].ig";
connectAttr "groupId2.id" "tweak1.ip[0].gi";
connectAttr "groupId2.msg" "tweakSet1.gn" -na;
connectAttr "m_tentacleShape.iog.og[5]" "tweakSet1.dsm" -na;
connectAttr "tweak1.msg" "tweakSet1.ub[0]";
connectAttr "m_tentacleShape1Orig.w" "groupParts2.ig";
connectAttr "groupId2.id" "groupParts2.gi";
connectAttr "skinCluster1GroupParts.og" "skinCluster1.ip[0].ig";
connectAttr "skinCluster1GroupId.id" "skinCluster1.ip[0].gi";
connectAttr "bindPose1.msg" "skinCluster1.bp";
connectAttr "joint1.wm" "skinCluster1.ma[0]";
connectAttr "joint2.wm" "skinCluster1.ma[1]";
connectAttr "joint3.wm" "skinCluster1.ma[2]";
connectAttr "joint4.wm" "skinCluster1.ma[3]";
connectAttr "joint1.liw" "skinCluster1.lw[0]";
connectAttr "joint2.liw" "skinCluster1.lw[1]";
connectAttr "joint3.liw" "skinCluster1.lw[2]";
connectAttr "joint4.liw" "skinCluster1.lw[3]";
connectAttr "joint1.obcc" "skinCluster1.ifcl[0]";
connectAttr "joint2.obcc" "skinCluster1.ifcl[1]";
connectAttr "joint3.obcc" "skinCluster1.ifcl[2]";
connectAttr "joint4.obcc" "skinCluster1.ifcl[3]";
connectAttr "joint4.msg" "skinCluster1.ptt";
connectAttr "skinCluster1GroupId.msg" "skinCluster1Set.gn" -na;
connectAttr "m_tentacleShape.iog.og[6]" "skinCluster1Set.dsm" -na;
connectAttr "skinCluster1.msg" "skinCluster1Set.ub[0]";
connectAttr "tweak1.og[0]" "skinCluster1GroupParts.ig";
connectAttr "skinCluster1GroupId.id" "skinCluster1GroupParts.gi";
connectAttr "joint1.msg" "bindPose1.m[0]";
connectAttr "joint2.msg" "bindPose1.m[1]";
connectAttr "joint3.msg" "bindPose1.m[2]";
connectAttr "joint4.msg" "bindPose1.m[3]";
connectAttr "bindPose1.w" "bindPose1.p[0]";
connectAttr "bindPose1.m[0]" "bindPose1.p[1]";
connectAttr "bindPose1.m[1]" "bindPose1.p[2]";
connectAttr "bindPose1.m[2]" "bindPose1.p[3]";
connectAttr "joint1.bps" "bindPose1.wm[0]";
connectAttr "joint2.bps" "bindPose1.wm[1]";
connectAttr "joint3.bps" "bindPose1.wm[2]";
connectAttr "joint4.bps" "bindPose1.wm[3]";
connectAttr "layerManager.dli[1]" "skeleton.id";
connectAttr "layerManager.dli[2]" "mesh.id";
connectAttr "skinCluster1.og[0]" "polyTweakUV1.ip";
connectAttr "m_tentacleShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of tentacle.ma
