﻿using UnityEngine;
using System.Collections;

public class InteractWithMayaIK : MonoBehaviour {

    public Animator animator;


    void Start()
    {
        animator.animatePhysics = true;
        animator.applyRootMotion = true;
    }

    void Update()
    {
        if(Input.GetKey(KeyCode.LeftArrow))
        {

            animator.SetIKPosition(AvatarIKGoal.LeftFoot, new Vector3(-100f, 0f, 0f));
            animator.SetIKPosition(AvatarIKGoal.RightFoot, new Vector3(-100f, 0f, 0f));
            animator.SetIKPosition(AvatarIKGoal.LeftHand, new Vector3(-100f, 0f, 0f));
            animator.SetIKPosition(AvatarIKGoal.RightHand, new Vector3(-100f, 0f, 0f));

        }else
        {
            animator.SetIKPosition(AvatarIKGoal.LeftFoot, new Vector3(100f, 0f, 0f));
            animator.SetIKPosition(AvatarIKGoal.RightFoot, new Vector3(100f, 0f, 0f));
            animator.SetIKPosition(AvatarIKGoal.LeftHand, new Vector3(100f, 0f, 0f));
            animator.SetIKPosition(AvatarIKGoal.RightHand, new Vector3(100f, 0f, 0f));
        }

    }
}
