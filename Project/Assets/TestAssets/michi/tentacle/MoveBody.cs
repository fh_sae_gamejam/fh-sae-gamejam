﻿using UnityEngine;
using System.Collections;

public class MoveBody : MonoBehaviour {

    public Vector3 position1;
    public Vector3 position2;
    public Rigidbody body;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetKey(KeyCode.RightArrow))
        {

            //body.MovePosition(new Vector3(10f, 0f, 0f));
            body.position = position1;
        }
        if(Input.GetKey(KeyCode.LeftArrow))
        {
            body.position = position2;
        }
	}
}
