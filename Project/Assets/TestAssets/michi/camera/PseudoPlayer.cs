﻿using UnityEngine;
using System.Collections;

public class PseudoPlayer : MonoBehaviour
{
    public float speed = 10f;


	void Start()
	{

	}

	void Update()
	{
		if (Input.GetKey(KeyCode.LeftArrow))
		{
            transform.position = transform.position + new Vector3(-Time.deltaTime, 0f, 0f) * speed;
		}
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.position = transform.position + new Vector3(Time.deltaTime, 0f, 0f) * speed;
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.position = transform.position + new Vector3(0f, Time.deltaTime, 0f) * speed;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.position = transform.position + new Vector3(0f, -Time.deltaTime, 0f) * speed;
        }
	}
}
