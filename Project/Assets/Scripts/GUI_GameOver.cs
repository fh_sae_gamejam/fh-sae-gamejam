﻿using UnityEngine;
using System.Collections;

public class GUI_GameOver : MonoBehaviour {

	private bool gameOver = false;
	public void GameOver() { gameOver = true; }

	public Texture bgTex;
	public GUIStyle style;

	private PlayerProperties pp;

	void Start() {
		pp = GameObject.Find("Player").GetComponent<PlayerProperties>();
	}

	void OnGUI() 
	{
		if (gameOver)
		{
			int w = 400;
			int h = 350;
			GUI.BeginGroup(new Rect(new Rect((Screen.width / 2) - w / 2, (Screen.height / 2) - h/2, w, h)));
			GUI.Box(new Rect(0, 0, w, h), "Game Over!", style);
			GUI.Label(new Rect(w / 4, h / 2 + 20, 50, 25), "Score: ", style);
			GUI.Label(new Rect(w / 4 + 150, h / 2 + 20, 50, 25), pp.GetPoints().ToString(), style);

			if (GUI.Button(new Rect(w / 4 - 25, h - 100, 100, 25), "Try Again"))
			{
				pp.AddPoints(0);
				Application.LoadLevel("main");
			}
			if (GUI.Button(new Rect(w / 4 + 125, h - 100, 100, 25), "Quit"))
				Application.Quit();
			GUI.EndGroup();
		}
	}
}
