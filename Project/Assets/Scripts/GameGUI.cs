﻿using UnityEngine;
using System.Collections;

public class GameGUI : MonoBehaviour {

	private GameObject player;

	private float time;
	private float maxtime;
	private float size;
	public float maxsize;

	private int points;

	public Texture2D bgImagetime; 
	public Texture2D fgImagetime;

	public Texture2D bgImagesize; 
	public Texture2D fgImagesize; 
	public Texture2D iconImagetime;
	public Texture2D iconImagesize;
    
	public float timeBarLength;
	public float sizeBarLength;

  public Texture2D fadeTex;
  private float alphaFade;
    
    void Start() {
		player = GameObject.Find("Player");
		time = 0;
		maxtime = 240;
		size = 1;
		maxsize = 15;
	}

	void Update () {

		if (Input.GetKeyDown(KeyCode.Escape))
			Application.Quit();

		points = player.GetComponent<PlayerProperties>().GetPoints();
		size = player.transform.localScale.magnitude;
		time = Time.timeSinceLevelLoad;
    }

  public void AddFadeoutAlpha()
  {
    alphaFade += Time.deltaTime * 0.2f;
    alphaFade = Mathf.Clamp01(alphaFade);
  }

	void OnGUI () {
		// Create one Group to contain both images
		// Adjust the first 2 coordinates to place it somewhere else on-screen
		GUI.BeginGroup (new Rect (0,0, timeBarLength+30,60));
		
		// Draw the background image
		GUI.DrawTexture (new Rect (0,0, timeBarLength,60), bgImagetime);
		
		// Create a second Group which will be clipped
		// We want to clip the image and not scale it, which is why we need the second Group
		GUI.BeginGroup (new Rect (0,0, time / maxtime * timeBarLength, 60));
		
		// Draw the foreground image
		GUI.DrawTexture (new Rect (0,0,timeBarLength,60), fgImagetime);

        
        // End both Groups
        GUI.EndGroup ();
		GUI.DrawTexture (new Rect (sizeBarLength, 0, 30,30), iconImagesize);
		GUI.EndGroup ();

		// Draw the foreground image



		GUI.BeginGroup (new Rect (0,0, sizeBarLength + 30,60));

		GUI.DrawTexture (new Rect (0,0, sizeBarLength,60), bgImagesize);

		GUI.BeginGroup (new Rect (0,0, size / maxsize * sizeBarLength, 60));
		GUI.DrawTexture (new Rect (0,0,sizeBarLength,60), fgImagesize);
        GUI.EndGroup ();

		GUI.DrawTexture (new Rect (sizeBarLength, 30, 30,30), iconImagetime);

        GUI.EndGroup ();
		GUI.Label(new Rect (Screen.width - 100,0, 100,100),"<size=40>"+points.ToString()+"</size>");
        
        GUI.color = new Vector4(GUI.color.r, GUI.color.g, GUI.color.b, alphaFade);
    	GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeTex);


        
    }
}