﻿using UnityEngine;
using System.Collections;

public class PlayerProperties : MonoBehaviour {

	public int Speed = 40;
	public float Damage = 0.02f;
	public float GrabDistance = 2;
	public float DamageResistance = 5.0f;
	public bool Invulnerability = false;
    private float size = 1;
	private static int points = 0;

  private float distance;

	// Use this for initialization
	void Start () {
    size = this.transform.localScale.magnitude;
	}
	
	// Update is called once per frame
	void Update () {
		GrabDistance = this.gameObject.transform.localScale.magnitude * 2;
    if ((this.transform.localScale.magnitude > size * 1.5) || (this.transform.localScale.magnitude < size * 1.5))
    {
      distance = 10 + this.transform.localScale.magnitude;
      if (distance > 28)
        distance = 30;

      Camera.main.orthographicSize = distance;
    }
	}

	public void AddPoints(int p) { points += p; if (p == 0) points = 0; }
	public int GetPoints() { return points;}

	public void ChangeSpeed(int value, float delay){
		StartCoroutine(ChangeSpeedCoroutine(value, delay));
	}

	IEnumerator ChangeSpeedCoroutine(int value, float delay) {
		this.Speed += value;
		yield return new WaitForSeconds(delay);
		this.Speed -= value;
	}

	public void ChangeDamage(float value, float delay){
		StartCoroutine(ChangeDamageCoroutine(value, delay));
	}

	IEnumerator ChangeDamageCoroutine(float value, float delay) {
		this.Damage += value;
		yield return new WaitForSeconds(delay);
		this.Damage -= value;
	}

	
	public void ChangeGrabDistance(float value, float delay){
		StartCoroutine(ChangeGrabDistanceCoroutine(value, delay));
	}
	
	IEnumerator ChangeGrabDistanceCoroutine(float value, float delay) {
		this.GrabDistance += value;
		yield return new WaitForSeconds(delay);
		this.GrabDistance -= value;
	}

	
	public void ChangeDamageResistance(float value, float delay){
		StartCoroutine(ChangeDamageResistanceCoroutine(value, delay));
	}
	
	IEnumerator ChangeDamageResistanceCoroutine(float value, float delay) {
		this.DamageResistance += value;
		yield return new WaitForSeconds(delay);
		this.DamageResistance -= value;
	}


	public void ChangeInvulnerability(float delay){
		StartCoroutine(ChangeInvulnerabilityCoroutine(delay));
	}
	
	IEnumerator ChangeInvulnerabilityCoroutine(float delay) {
		this.Invulnerability = true;
		yield return new WaitForSeconds(delay);
		this.Invulnerability = false;
	}
}
