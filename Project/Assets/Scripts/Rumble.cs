﻿using UnityEngine;
using System.Collections;

public class Rumble : MonoBehaviour {

	public int secondsToFirstRumble = 5;
	public int playFirstXTimes = 4;
	private int playedFirstFor = 0;

	public int secondsToSecondRumble = 15;
	public int playSecondXTimes = 6;
	private int playedSecondFor = 0;

	public int secondsToThirdRumble = 25;
	public int playThirdXTimes = 15;
	private int playedThirdFor = 0;

	public int secondsToEnd = 35;

	public float baseRumbleForce = 1.0f;

	public AudioClip bedCreak;
	public AudioClip bedCreakEnd;

	private bool gameOver = false;
	public void GameOver() { gameOver = true; }

	private bool firstRumbleDone = false;
	private bool secondRumbleDone = false;
	private bool thirdRumbleDone = false;
	private bool endDone = false;

	private float timeSinceStart = 0;

	private enum RumbleStrength
	{
		Weak,
		Middle,
		Strong
	};

	private AnimationState state;

	private float fadeOutTimer = 0.0f;
	
	void RumbleEnemies(RumbleStrength strength)
	{
		// get all enemies
		GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

		// get player
		GameObject player = GameObject.Find("Player");

		float rX, rY;

		switch (strength)
		{
		case RumbleStrength.Weak:
			rX = Random.Range(-baseRumbleForce / 2, baseRumbleForce / 2);
			rY = Random.Range(-baseRumbleForce / 2, baseRumbleForce / 2);

			player.rigidbody.AddForce(new Vector3(rX, rY, 0), ForceMode.Impulse);

			for (int i = 0; i < enemies.Length; i++)
			{
        if (enemies[i].activeSelf)
        {
          rX = Random.Range(-baseRumbleForce / 4, baseRumbleForce / 4);
          rY = Random.Range(-baseRumbleForce / 4, baseRumbleForce / 4);

          enemies[i].rigidbody.AddForce(new Vector3(rX, rY, 0), ForceMode.Impulse);
        }
			}

			break;
		case RumbleStrength.Middle:
			rX = Random.Range(-baseRumbleForce, baseRumbleForce);
			rY = Random.Range(-baseRumbleForce, baseRumbleForce);
			
			player.rigidbody.AddForce(new Vector3(rX, rY, 0), ForceMode.Impulse);

			for (int i = 0; i < enemies.Length; i++)
			{
        if (enemies[i].activeSelf)
        {
          rX = Random.Range(-baseRumbleForce / 2, baseRumbleForce / 2);
          rY = Random.Range(-baseRumbleForce / 2, baseRumbleForce / 2);

          enemies[i].rigidbody.AddForce(new Vector3(rX, rY, 0), ForceMode.Impulse);
        }
			}

			break;
		case RumbleStrength.Strong:
			rX = Random.Range(-baseRumbleForce * 2, baseRumbleForce * 2);
			rY = Random.Range(-baseRumbleForce * 2, baseRumbleForce * 2);
			
			player.rigidbody.AddForce(new Vector3(rX, rY, 0), ForceMode.Impulse);

			for (int i = 0; i < enemies.Length; i++)
			{
        if (enemies[i].activeSelf)
        {
          rX = Random.Range(-baseRumbleForce, baseRumbleForce);
          rY = Random.Range(-baseRumbleForce, baseRumbleForce);

          enemies[i].rigidbody.AddForce(new Vector3(rX, rY, 0), ForceMode.Impulse);
        }
			}

			break;
		}
	}

	void Update() 
	{
		if (!gameOver)
		{
			timeSinceStart += Time.deltaTime;

			if (timeSinceStart >= secondsToEnd && !endDone)
			{
				endDone = true;
				GameObject.Find("Player").GetComponent<PlayerProperties>().Invulnerability = true;
				StartCoroutine("FadeOut");
			}
			else if (timeSinceStart >= secondsToThirdRumble && !thirdRumbleDone)
			{
				if(!Camera.main.audio.isPlaying)
				{
					Camera.main.audio.clip = this.bedCreak;
					Camera.main.audio.Play();
	            }

				state = Camera.main.animation.PlayQueued("Rumble_L");
				state.speed = 1.8f;
				RumbleEnemies(RumbleStrength.Strong);

				playedThirdFor++;
				if (playedThirdFor == playThirdXTimes)
				{
					Camera.main.animation.PlayQueued("Rumble_Fadeout_L");
					thirdRumbleDone = true;
				}
			}
			if (timeSinceStart >= secondsToSecondRumble && !secondRumbleDone)
			{
				if(!Camera.main.audio.isPlaying)
				{
					Camera.main.audio.clip = this.bedCreak;
					Camera.main.audio.Play();
				}

	            state = Camera.main.animation.PlayQueued("Rumble_L");
				state.speed = 1.2f;
				RumbleEnemies(RumbleStrength.Middle);

				playedSecondFor++;
				if (playedSecondFor == playSecondXTimes)
				{
					Camera.main.animation.PlayQueued("Rumble_Fadeout_L");
					secondRumbleDone = true;
				}

			}
			else if (timeSinceStart >= secondsToFirstRumble && !firstRumbleDone)
			{
				if(!Camera.main.audio.isPlaying)
				{
					Camera.main.audio.clip = this.bedCreak;
					Camera.main.audio.Play();
	            }

				Camera.main.animation.PlayQueued("Rumble_L");
				RumbleEnemies(RumbleStrength.Weak);

				playedFirstFor++;
				if (playedFirstFor == playFirstXTimes)
				{
					Camera.main.animation.PlayQueued("Rumble_Fadeout_L");
					firstRumbleDone = true;
				}

			}
		}
	}

	IEnumerator FadeOut()
	{
		if (GameObject.Find("Player").transform.localScale.magnitude >=
		    GameObject.Find("UICamera").GetComponent<GameGUI>().maxsize )
		{
			while (fadeOutTimer < 5.0f)
			{
				fadeOutTimer += Time.deltaTime;
				GameObject.Find("UICamera").GetComponent<GameGUI>().AddFadeoutAlpha();

				yield return null;
			}
				Application.LoadLevel("main");
		}
		else
		{
			Camera.main.GetComponent<Rumble>().GameOver();
			Camera.main.GetComponent<GUI_GameOver>().GameOver();
		}
	}
}
