﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {

  [SerializeField]
  float speed = 10.0f;
  [SerializeField]
  private float rotX;
  [SerializeField]
  private float rotY;
  [SerializeField]
  private float rotZ;
  private Vector3 rotVector;
	// Use this for initialization
	void Start () {
    rotX = 0; // (float)Random.Range(-100, 100) / 100;
    rotY = (float)Random.Range(-20, 20) / 100;
    rotZ = (float)Random.Range(-100, 100) / 100;
    rotVector = new Vector3(rotX, rotY, rotZ);
	}
	
	// Update is called once per frame
	void Update () {
    transform.Rotate(rotVector * Time.deltaTime * speed);
	}
}
