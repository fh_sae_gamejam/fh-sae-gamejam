﻿using UnityEngine;
using System.Collections;

public class UpdateEnemySize : MonoBehaviour {
  [SerializeField] float size = 1.0f;
  private Transform target;
	// Use this for initialization
	void Start () {
    target = Camera.main.GetComponent<SmoothFollow2d>().target;
    float temp = target.transform.localScale.magnitude * size;
    if (temp < 0.2)
      temp = 0.2f;
    this.transform.localScale = new Vector3(1, 1, 1) *temp;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
