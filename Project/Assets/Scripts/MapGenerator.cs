﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapGenerator : MonoBehaviour {

  [SerializeField] int amount = 50;
  [SerializeField] float minRadius = 22.0f;
  [SerializeField] float maxRadius = 60.0f;
  [SerializeField] int Bigamount = 4;
  [SerializeField] float BigminRadius = 30.0f;
  [SerializeField] float BigmaxRadius = 160.0f;
  [SerializeField] GameObject[] createObjects;
  [SerializeField] GameObject[] createObjectsBig;

  private GameObject player;

  public int[] spawnChance;

  private StablePointsGenerator generator;
  private StablePointsGenerator generatorBig;

  private Dictionary<Vector3, GameObject> dictionary;
  private Dictionary<Vector3, GameObject> dictionaryBig;

  private float playerSize;
	// Use this for initialization

	void Start () {
    generatorBig = new StablePointsGenerator(BigmaxRadius, BigminRadius, Bigamount);
    generator = new StablePointsGenerator(15, 3, 10);
    dictionary = new Dictionary<Vector3, GameObject>();
    dictionaryBig = new Dictionary<Vector3, GameObject>();

    player = Camera.main.GetComponent<SmoothFollow2d>().target.gameObject;
    GameObject go;
    IEnumerable<Vector3> points = generator.GetNewPoints(player.transform.position);
    foreach (Vector3 point in points)
    {
      go = (GameObject)Instantiate(createObjects[getObjectIndex()], point, Quaternion.identity);
      float scale = getObjectSize();
      go.transform.localScale *= scale;
      dictionary.Add(point, go);
    }
    generator.setValues(minRadius, maxRadius, amount);
	}
	
	// Update is called once per fraem
	void Update () {
    GameObject go;
    IEnumerable<Vector3> points = generator.GetNewPoints(player.transform.position);
    foreach (Vector3 point in points)
    {
      go = (GameObject)Instantiate(createObjects[getObjectIndex()], point, Quaternion.identity);
      dictionary.Add(point, go);
      float scale = getObjectSize();
      go.transform.localScale *= scale;
    }

    points = generator.GetLastDeletedPoints();
    foreach (Vector3 point in points)
    {
      go = dictionary[point];
      Destroy(go);
    }

    //Generate Big Stuff
    points = generatorBig.GetNewPoints(player.transform.position);
    foreach (Vector3 point in points)
    {
      go = (GameObject)Instantiate(createObjectsBig[getBigObjectIndex()], point, Quaternion.identity);
      dictionaryBig.Add(point, go);
    }
    points = generatorBig.GetLastDeletedPoints();
    foreach (Vector3 point in points)
    {
      go = dictionaryBig[point];
      Destroy(go);
    }
	}

  int getObjectIndex()
  {
    int value = Random.Range(0, 100);
    int objectIndex = -1;
    for (int i = 0; i < spawnChance.Length; i++)
    {
      if (value <= spawnChance[i])
      {
        objectIndex = i;
        break;
      }
    }
    return objectIndex;
  }

  int getBigObjectIndex()
  {
    int value = Random.Range(0, createObjectsBig.Length);
    return value;
  }

  float getObjectSize()
  {
    float objectSize;
    int value = Random.Range(0, 100);
    if (value <= 10)
      objectSize = 0.3f;
    else if (value <= 20)
      objectSize = 0.6f;
    else if (value <= 40)
      objectSize = 0.7f;
    else if (value <= 60)
      objectSize = 0.9f;
    else if (value <= 70)
      objectSize = 1.05f;
    else if (value <= 80)
      objectSize = 0.8f;
    else if (value <= 1.3f)
      objectSize = 1.6f;
    else if (value <= 95)
      objectSize = 2.0f;
    else if (value <= 100)
      objectSize = 2.8f;
    else objectSize = 0.9f;
    return objectSize;
  }
}
