﻿using UnityEngine;
using System.Collections;

public class CollisionDetection : MonoBehaviour {

	public float Damage = 0.02f;
	public float DamageResistace = 5.0f;
  public float killPlayerSize = 0.4f;
  public float killEnemySize = 0.3f;
	public bool Invulnerability = false;
	private PlayerProperties pp;

	void Start()
	{
		pp = GameObject.Find("Player").GetComponent<PlayerProperties>();
	}

	void OnTriggerStay(Collider collision)	{
		Damage = pp.Damage;
		DamageResistace = pp.DamageResistance;
		Invulnerability = pp.Invulnerability;

		if (collision.gameObject.layer == 9)
		{					
			EnemyScript es = collision.gameObject.GetComponent<EnemyScript>();
			if(es != null)
			{
				float myScale = this.gameObject.transform.localScale.magnitude;
				float enemyScale = collision.gameObject.transform.localScale.magnitude * es.Volume;
				if(enemyScale <= myScale)
				{
					pp.AddPoints(1);
					this.audio.Play();

					collision.transform.localScale -= new Vector3(Damage,Damage,Damage);
          if (collision.transform.localScale.x <= killEnemySize || collision.transform.localScale.y <= killEnemySize || collision.transform.localScale.z <= killEnemySize)
					{
						//Buff first, destroy later
						switch(es.Type)
						{
						case BuffType.DAMAGE_BUFF:
							pp.ChangeDamage(5.0f,5.0f);
							break;
						case BuffType.GRAB_BUFF:
							pp.ChangeGrabDistance(5.0f,5.0f);
							break;
						case BuffType.INVULNERABLE_BUFF:
							pp.ChangeInvulnerability(5.0f);
							break;
						case BuffType.RESISTANCE_BUFF:
							pp.ChangeDamageResistance(3.0f,5.0f);
							break;
						case BuffType.SPEED_BUFF:
							pp.ChangeSpeed(20,5.0f);
							break;
						default:
							break;
						}
            collision.gameObject.SetActive(false);
					}
					if(!Invulnerability && DamageResistace != 0) {
						this.transform.localScale += new Vector3(Damage,Damage,Damage)/DamageResistace;
					}
				}
				else
				{
					collision.audio.Play();
					pp.AddPoints(-2);

					collision.transform.localScale += new Vector3(Damage,Damage,Damage)/5.0f;

					if(!Invulnerability) {
						this.transform.localScale -= new Vector3(Damage,Damage,Damage);
					}

          if (this.transform.localScale.x <= killPlayerSize || this.transform.localScale.y <= killPlayerSize || this.transform.localScale.z <= killPlayerSize)
					{
						this.gameObject.SetActive(false);
						Camera.main.GetComponent<Rumble>().GameOver();
						Camera.main.GetComponent<GUI_GameOver>().GameOver();
	                }
	            }
			}
		}
		else if (collision.gameObject.layer == 10 && !Invulnerability )
		{
			collision.audio.Play();
			pp.AddPoints(-2);

			this.transform.localScale -= new Vector3(Damage,Damage,Damage);

      if (this.transform.localScale.x <= killPlayerSize || this.transform.localScale.y <= killPlayerSize || this.transform.localScale.z <= killPlayerSize)
			{
				this.gameObject.SetActive(false);
				Camera.main.GetComponent<Rumble>().GameOver();
				Camera.main.GetComponent<GUI_GameOver>().GameOver();
            }
        }
    }

	
}
