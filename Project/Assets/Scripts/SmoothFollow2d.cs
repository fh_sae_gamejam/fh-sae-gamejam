﻿using UnityEngine;

public class SmoothFollow2d : MonoBehaviour
{
    public Transform target;
    public float damping = 5f;

    void Update()
    {
        Vector3 targetPosition = target.transform.position;
        Vector2 wanted = new Vector2(targetPosition.x, targetPosition.y);
        Vector2 current = transform.position;
        current = new Vector2(
            Mathf.Lerp(current.x, wanted.x, Time.deltaTime * damping),
            Mathf.Lerp(current.y, wanted.y, Time.deltaTime * damping));
        transform.position = new Vector3(current.x, current.y, transform.position.z);
    }
   
}