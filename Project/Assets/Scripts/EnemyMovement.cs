﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour {

	public float speed = 1.0f;
	public float radius = 3.0f;

	private Transform player;

	private enum MovementType 
	{
		Impulse,
		Steady
	};
	private MovementType myMovement = MovementType.Steady;

	private EnemyScript myProps;

	private float slowDownBy = 0.001f;
	private float impulseTimer = 0;

	void Start()
	{
		if (this.name == "Enemy 2" || this.name == "Enemy 3"
		    || this.name == "Enemy 2(Clone)" || this.name == "Enemy 3(Clone")
			this.myMovement = MovementType.Impulse;

		player = GameObject.Find("Player").transform;
		myProps = this.GetComponent<EnemyScript>();
	}

	void Update () 
	{
		if (myMovement == MovementType.Impulse)
		{
			// loose velocity if impulse
			float newX = this.rigidbody.velocity.x - slowDownBy;
			float newY = this.rigidbody.velocity.y - slowDownBy;
			if (this.rigidbody.velocity.x < 0)
				newX += slowDownBy * 2;
			if (this.rigidbody.velocity.y < 0)
				newY += slowDownBy * 2;

			this.rigidbody.velocity = new Vector3(newX, newY, 0);

			impulseTimer += Time.deltaTime;

			if (impulseTimer > 5.0f)
			{
				impulseTimer = 0;
				Vector3 force;

				// check if target within radius and only target if we are bigger
				if (Vector3.Distance(this.transform.position, this.player.position) < radius
				    && myProps.Volume * this.transform.localScale.x > this.player.transform.localScale.x
				    && this.player.gameObject.activeSelf)
				{
					Vector3 vec = (this.player.position - this.transform.position).normalized;
					force = vec * speed;
					force.z = 0;
				}
				else
				{
					float rX = Random.Range(-speed, speed);
					float rY = Random.Range(-speed, speed);
					force = new Vector3(rX, rY, 0);
				}

				this.rigidbody.AddForce(force, ForceMode.Impulse);
			}
		}
		else
		{
			float rX, rY;

			if (Vector3.Distance(this.transform.position, this.player.position) < radius
			    && myProps.Volume * this.transform.localScale.x > this.player.transform.localScale.x
			    && this.player.gameObject.activeSelf)
			{
				Vector3 vec = (this.player.position - this.transform.position).normalized;
				rX = vec.x * speed;
				rY = vec.y * speed;
			}
			else
			{
				rX = Random.Range(-speed, speed);
				rY = Random.Range(-speed, speed);
			}

			this.rigidbody.AddForce(new Vector3(rX, rY, 0), ForceMode.VelocityChange);
		}
	}
}
