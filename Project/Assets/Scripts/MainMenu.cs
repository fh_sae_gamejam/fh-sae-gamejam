﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	public Texture2D background;
	public Texture2D buttonTex;

	private GUIStyle labelStyle;

	public int width = 50;
	public int height = 200;

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
			Application.Quit();
	}

	void OnGUI()
	{
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), background);

		// rotate
		// float rotAngle = 0;
		// Vector2 pivotPoint;
		// pivotPoint = new Vector2(100, 25);
		// GUIUtility.RotateAroundPivot(rotAngle, pivotPoint);

		if(GUI.Button(new Rect(30, Screen.height / 2 - height / 2 - 10, width, height), ""))
			Application.LoadLevel("main");
		GUI.DrawTexture(new Rect(30, Screen.height / 2 - height / 2 - 10, width, height), buttonTex);
	}
}
