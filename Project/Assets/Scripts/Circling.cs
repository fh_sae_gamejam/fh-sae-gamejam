﻿using UnityEngine;

public class Circling : MonoBehaviour
{
	/* speed of orbit (in degrees/second) */
	public float speed;   
	public Vector3 target;
	
	void Update()
	{
		if (speed > 0) {
			transform.RotateAround(target, Vector3.up, speed * Time.deltaTime);
		}
	}
}