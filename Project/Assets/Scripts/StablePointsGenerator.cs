﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

 public class StablePointsGenerator
    {
        private float _maxRadius;
        private int _amount;
        private float _minRadius;
        private readonly List<Vector3> _currentPoints;
        private Vector3? _lastCenter;
        private List<Vector3> _deletedPoints;
        public StablePointsGenerator(float maxRadius, float minRadius, int amount)
        {
            _maxRadius = maxRadius;
            _minRadius = minRadius;
            _amount = amount;

            _currentPoints=new List<Vector3>();
        }

        public void setValues(float minRadius, float maxRadius, int amount)
        {
          _minRadius = minRadius;
          if (_minRadius >= _maxRadius)
            _maxRadius += 5;
          _maxRadius = maxRadius;
          _amount = amount;

        }

        public IEnumerable<Vector3> GetNewPoints(Vector3 center)
        {
            //delete points outside new center
            _deletedPoints = DeletePoints(_currentPoints, point => Vector3.Distance(center, point) > _maxRadius).ToList();

            var remainingPoints = _amount - _currentPoints.Count;
            var newPoints = new List<Vector3>();
            while (remainingPoints > 0)
            {
                var possibleNewPoints = GeneratePoints(center, remainingPoints, _maxRadius, _minRadius).ToList();
                
                if (_lastCenter.HasValue)
                {
                    //remove points inside old center (we dont want to overwrite them)
                    var lastCenter = _lastCenter.Value;
                    DeletePoints(possibleNewPoints, point => Vector3.Distance(lastCenter, point) < _maxRadius);
                }

                remainingPoints -= possibleNewPoints.Count;
                newPoints.AddRange(possibleNewPoints);
            }

            _currentPoints.AddRange(newPoints);

            _lastCenter = center;
          return newPoints;
        }

        public IEnumerable<Vector3> GetLastDeletedPoints()
        {
            return _deletedPoints;
        }

        private static IEnumerable<Vector3> GeneratePoints(Vector3 center, float pointsToGenerate, float radius, float minRadius)
        {
          bool distance = false;
          var newPoint = center;
            for (var i = 0; i < pointsToGenerate; i++)
            {
              distance = false;
              while(distance == false)
              {
                Vector2 pos = new Vector2();
                pos = Random.insideUnitCircle * radius;
                Vector3 newPos = center + new Vector3(pos.x,pos.y,0);

                if (Vector3.Distance(newPos, center) > minRadius)
                {
				          newPoint = newPos;
                  distance = true;
                }
              }
              yield return newPoint;
            }
        }

        private static IEnumerable<Vector3> DeletePoints(IList<Vector3> points, Func<Vector3, bool> condition)
        {
            for (var i = 0; i < points.Count; i++)
            {
                if (!condition(points[i]))
                    continue;

                yield return points[i];

                points.RemoveAt(i);
                i--;
            }
        }
    }
