﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GrapMovement : MonoBehaviour {

	public float forceMultiplier = 2;
	public float minClickLengthMod = 1.0f;
	public float maxClickLengthMod = 5.0f;
	public float slowDownBy = 0.001f;
	public float maxVelocityThreshold = 5.0f;
	public float minVelocityThreshold = 0.8f;
	public float clickThreshold = 0.4f;
    public List<Tentacle> tentacles;


	private float clickLength = 0.0f;
	private float lastClick = 0.0f;

	private PlayerProperties pp;


	private RaycastHit currHit;
	private bool hitSomething = false;

	void Start() 
	{

		pp = GameObject.Find("Player").GetComponent<PlayerProperties>();
    }

	void Update() 
	{
		if (this.gameObject.activeSelf)
		{
			/* MOVEMENT */

			//get actual Speed
			//forceMultiplier = pp.Speed;
			float localForce = forceMultiplier * pp.Speed;

			float currX = this.rigidbody.velocity.x;
			float currY = this.rigidbody.velocity.y;

			// get slower over time
			if (this.rigidbody.velocity.magnitude > minVelocityThreshold)
			{
				float newX = currX - slowDownBy;
				float newY = currY - slowDownBy;
				if (this.rigidbody.velocity.x < 0)
					newX += slowDownBy * 2;
				if (this.rigidbody.velocity.y < 0)
					newY += slowDownBy * 2;

				this.rigidbody.velocity.Set(newX, newY, 0);            
	        }

			// make sure that we are not going too fast
			if (Mathf.Abs(currX) > maxVelocityThreshold)
			{
				if (Mathf.Sign(currX) < 0)
					currX = -maxVelocityThreshold;
				else
					currX = maxVelocityThreshold;
			}
			if (Mathf.Abs(currY) > maxVelocityThreshold)
			{
				if (Mathf.Sign(currY) < 0)
					currY = -maxVelocityThreshold;
				else
					currY = maxVelocityThreshold;
			}
			this.rigidbody.velocity = new Vector3(currX, currY, 0);


			/* GRAB */

			// update last click
			lastClick += Time.deltaTime;

	        // how long did player click
			if (hitSomething && lastClick > clickThreshold)
				clickLength += Time.deltaTime;

			if (Input.GetMouseButtonDown(0) && lastClick > clickThreshold)
			{
				// get the thing we clicked
				Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);

				if (Physics.Raycast(r, out currHit, 100)) 
				{
                    _GetNearestTentacle(currHit.point).StartGrab(currHit.point);
					// we hit something, now we have to check if something 
					// is between us and our hit
					Vector3 dir = currHit.point - transform.position;
					
					Ray newR = new Ray(transform.position, dir);
					RaycastHit newH;
					if (Physics.Raycast(newR, out newH, 100))
					{
						currHit = newH;
					}
					hitSomething = true;
				}
			}

			// move somewhere if clicked
			if (Input.GetMouseButtonUp(0) && hitSomething)
			{				Vector3 dir = currHit.point - transform.position;

				// apply force
				clickLength = Mathf.Clamp(clickLength, minClickLengthMod, maxClickLengthMod);
				Vector3 force = dir.normalized * localForce * (clickLength * 2);

				this.rigidbody.AddForceAtPosition(force, _GetNearestTentacle(currHit.point).transform.position, ForceMode.Force);

				lastClick = 0.0f;
				clickLength = 0.0f;
				hitSomething = false;
			}
		}
	}

    private Tentacle _GetNearestTentacle(Vector3 positionTo)
    {
        int closestIndex = 0;
        for (int i = 0; i < this.tentacles.Count; i++)
        {
            float distance = (this.tentacles[i].transform.position - positionTo).sqrMagnitude;
            float closestDistance = (this.tentacles[closestIndex].transform.position - positionTo).sqrMagnitude;
            if (distance < closestDistance)
            {
                closestIndex = i;
            }
        }
        return tentacles[closestIndex];
    }
}
