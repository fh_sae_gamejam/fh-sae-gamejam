﻿using UnityEngine;
using System.Collections;

public enum BuffType {NO_BUFF=0, SPEED_BUFF, GRAB_BUFF, DAMAGE_BUFF, RESISTANCE_BUFF, INVULNERABLE_BUFF};

public class EnemyScript : MonoBehaviour {


	public BuffType Type = 0;
	public float Volume = 1;
  public float rotationY = 0.0f;
  private Transform Player;
  private float bigger;
  private float newBigger;

  public float Damage = 0.02f;
  public float DamageResistace = 5.0f;
  public float killPlayerSize = 0.4f;
  public float killEnemySize = 0.3f;
  public bool Invulnerability = false;

	// Use this for initialization
	void Start () {
    this.transform.Rotate(new Vector3(0, rotationY,0));
    Player = Camera.main.GetComponent<SmoothFollow2d>().target;
		//20 percent probability that there actually is a buff
    if (Random.Range(0.0f, 1.0f) < 0.8f)
    {
      Type = BuffType.NO_BUFF;
    }
    else
    {
      Behaviour h = (Behaviour)GetComponent("Halo");
      if (h)
        h.enabled = true;
    }
		if(Volume <= 0)
		{
			Volume = 0.0001f;
		}
	}
	
	// Update is called once per frame
	void Update () {
    newBigger = this.Volume - Player.localScale.magnitude;
    if(newBigger != bigger)
    {
      //Todo
    }
	}

  void OnTriggerStay(Collider collision)
  {
    if (collision.gameObject.layer == 9)
    {
      EnemyScript es = collision.gameObject.GetComponent<EnemyScript>();
      if (es != null)
      {
        float myScale = this.gameObject.transform.localScale.magnitude;
        float enemyScale = collision.gameObject.transform.localScale.magnitude * es.Volume;
        if (enemyScale <= myScale)
        {
          //this.audio.Play();

          collision.transform.localScale -= new Vector3(Damage, Damage, Damage);
          if (collision.transform.localScale.x <= killEnemySize || collision.transform.localScale.y <= killEnemySize || collision.transform.localScale.z <= killEnemySize)
          {
            collision.gameObject.SetActive(false);
          }
            this.transform.localScale += new Vector3(Damage, Damage, Damage) / DamageResistace;
        }
        else
        {
          //collision.audio.Play();

          collision.transform.localScale += new Vector3(Damage, Damage, Damage) / 5.0f;

          if (!Invulnerability)
          {
            this.transform.localScale -= new Vector3(Damage, Damage, Damage);
          }

          if (this.transform.localScale.x <= killEnemySize || this.transform.localScale.y <= killEnemySize || this.transform.localScale.z <= killEnemySize)
          {
            this.gameObject.SetActive(false);
          }
        }
      }
    }
    else if (collision.gameObject.layer == 10 && !Invulnerability)
    {
      collision.audio.Play();
      this.transform.localScale -= new Vector3(Damage, Damage, Damage);

      if (this.transform.localScale.x <= killEnemySize || this.transform.localScale.y <= killEnemySize || this.transform.localScale.z <= killEnemySize)
      {
        this.gameObject.SetActive(false);
      }
    }
  }
}
