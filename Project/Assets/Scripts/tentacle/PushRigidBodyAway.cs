﻿using UnityEngine;
using System.Collections;

public class PushRigidBodyAway : MonoBehaviour {

    public Rigidbody toPush;
    public GameObject pusher;
    public float force;
    public float interval;

    void Start()
    {
        StartCoroutine(Push());
    }

    IEnumerator Push()
    {
        while(true)
        {
            Vector3 vector = (toPush.worldCenterOfMass - pusher.transform.position).normalized
                * force;
            toPush.AddForce(vector);
            yield return new WaitForSeconds(interval);
        }
    }
}
