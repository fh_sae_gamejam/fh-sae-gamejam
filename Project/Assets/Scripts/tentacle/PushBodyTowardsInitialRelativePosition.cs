﻿using UnityEngine;
using System.Collections;

public class PushBodyTowardsInitialRelativePosition : MonoBehaviour {

    public Rigidbody toPush = null;
    public GameObject pusher = null;
    public float force = 1f;
    public float interval = 1f;

    private Vector3 _InitialRelativePosition;

    void Awake()
    {
        _InitialRelativePosition = toPush.worldCenterOfMass - pusher.transform.position;
    }

    void Start()
    {
        StartCoroutine(Push());
    }

    IEnumerator Push()
    {
        while(true)
        {
            Vector3 newRelativePosition = (toPush.worldCenterOfMass - pusher.transform.position);
            Vector3 toInitialRelativePosition = _InitialRelativePosition - newRelativePosition;
            toPush.AddForce(toInitialRelativePosition * Mathf.Sqrt(force));

            yield return new WaitForSeconds(interval);
        }
    }
}
