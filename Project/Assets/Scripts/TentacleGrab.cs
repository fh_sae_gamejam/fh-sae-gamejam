﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TentacleGrab : MonoBehaviour {

	public float forceMultiplier = 2;
	public float minClickLengthMod = 1.0f;
	public float maxClickLengthMod = 5.0f;
	public float slowDownBy = 0.001f;
	public float maxVelocityThreshold = 5.0f;
	public float minVelocityThreshold = 0.8f;
	public float clickThreshold = 0.4f;

    public List<Tentacle> tentacles;
    public float grabDistance = 5f;
    public float grabDuration = 0.5f;


	private float clickLength = 0.0f;
	private float lastClick = 0.0f;

	private PlayerProperties playerProperties;
    private Tentacle _GrabbingTentacle;

	void Start() 
	{
		playerProperties = GameObject.Find("Player").GetComponent<PlayerProperties>();
        foreach(Tentacle tentacle in tentacles)
        {
            tentacle.grabDuration = grabDuration;
        }
    }

	void Update() 
	{
		if (this.gameObject.activeSelf)
		{
			float localForce = forceMultiplier * playerProperties.Speed;
			float currX = this.rigidbody.velocity.x;
			float currY = this.rigidbody.velocity.y;

			if (this.rigidbody.velocity.magnitude > minVelocityThreshold)
			{
				float newX = currX - slowDownBy;
				float newY = currY - slowDownBy;
				if (this.rigidbody.velocity.x < 0)
					newX += slowDownBy * 2;
				if (this.rigidbody.velocity.y < 0)
					newY += slowDownBy * 2;

				this.rigidbody.velocity.Set(newX, newY, 0);            
	        }

			if (Mathf.Abs(currX) > maxVelocityThreshold)
			{
				if (Mathf.Sign(currX) < 0)
					currX = -maxVelocityThreshold;
				else
					currX = maxVelocityThreshold;
			}
			if (Mathf.Abs(currY) > maxVelocityThreshold)
			{
				if (Mathf.Sign(currY) < 0)
					currY = -maxVelocityThreshold;
				else
					currY = maxVelocityThreshold;
			}
			this.rigidbody.velocity = new Vector3(currX, currY, 0);

			lastClick += Time.deltaTime;
           
            if(_GrabbingTentacle != null && _GrabbingTentacle.State == TentacleState.Free)
            {
                _GrabbingTentacle = null;
            }

            if (Input.GetMouseButtonUp(0))
			{
                if(_GrabbingTentacle != null)
                {
                    switch (_GrabbingTentacle.State) {
                        case TentacleState.Grabbing:
                            _GrabbingTentacle.InterruptGrab();
                            break;
                        case TentacleState.Attached:
                            Vector3 moveDirection = (_GrabbingTentacle.tip.transform.position - _GrabbingTentacle.transform.position).normalized;
                            clickLength = Mathf.Clamp(clickLength, minClickLengthMod, maxClickLengthMod);
                            Vector3 force = moveDirection * localForce * Mathf.Pow(clickLength, 2f);
                            rigidbody.AddForceAtPosition(force, _GrabbingTentacle.transform.position, ForceMode.Force);
                            foreach(Tentacle tentacle in tentacles)
                            {
                                tentacle.Detach();
                            }
                            break;
                        default:
                            throw new UnityException("this code ought not to be reachable");
                    }
                    lastClick = 0.0f;
                    clickLength = 0.0f;
                }
			}

            if (Input.GetMouseButtonDown(0) && lastClick > clickThreshold && _GrabbingTentacle == null)
            {     
                Vector3 mouseWorldPosition = Input.mousePosition;
                mouseWorldPosition = Camera.main.ScreenToWorldPoint(mouseWorldPosition);
                Tentacle nearestTentacle = _GetNearestTentacle(mouseWorldPosition);
                mouseWorldPosition.z = nearestTentacle.transform.position.z;
                Vector3 target = nearestTentacle.transform.position +
                    (mouseWorldPosition - nearestTentacle.transform.position).normalized
                        * grabDistance * transform.lossyScale.magnitude;
                if(nearestTentacle.StartGrab(target))
                {
                    _GrabbingTentacle = nearestTentacle;
                }
            }
		}
	}

    private Tentacle _GetNearestTentacle(Vector3 positionTo)
    {
        int closestIndex = -1;
        float closestDistance = float.MaxValue;
        for (int i = 0; i < tentacles.Count; ++i)
        {
            float distance = (tentacles[i].transform.position - positionTo).sqrMagnitude;
            if (distance < closestDistance)
            {
                closestDistance = distance;
                closestIndex = i;
            }
        }
        return tentacles[closestIndex];
    }
}
