﻿using UnityEngine;
using System;

internal enum QuarterIndex
{
    UpperLeft = 0,
    UpperRight = 1,
    LowerLeft = 2,
    LowerRight = 3,
}

internal struct IntVector2
{
    public int x;
    public int y;
}

public class BackGroundMove : MonoBehaviour {

	public Transform player;
	public GameObject backGround;
	public float backGroundWidth;
	public float backGroundHeight;
    public float backGroundZ;

	public Color[] colors;
	private int index = 0;

    private GameObject[] _BackGroundInstances;
    private IntVector2 _GridIndizes;

    void Awake () {

		index = UnityEngine.Random.Range(0, colors.Length);

        const int NUM_BACKGROUND_INSTANCES = 4;
        _BackGroundInstances = new GameObject[NUM_BACKGROUND_INSTANCES];
        for(int i = 0; i < NUM_BACKGROUND_INSTANCES; ++i)
        {
            _BackGroundInstances[i] = (GameObject)GameObject.Instantiate(backGround);
			_BackGroundInstances[i].renderer.material.SetColor("_Color", colors[index]);
        }
        _GridIndizes = _GetGridIndizes();
        ApplyGridIndizes(_GridIndizes);
    }

	void Update () {
        _GridIndizes = _GetGridIndizes();
        ApplyGridIndizes(_GridIndizes);
	}

    private IntVector2 _GetGridIndizes()
    {
        IntVector2 gridPosition = new IntVector2();
        gridPosition.x = (int)Math.Floor((transform.position.x / backGroundWidth)); 
        gridPosition.y =(int)Math.Floor((transform.position.y / backGroundHeight));
        return gridPosition;
    }

    void ApplyGridIndizes(IntVector2 gridIndizes)
    {
        IntVector2 position = gridIndizes;
        ApplyGridIndizes(position, _BackGroundInstances[(int)QuarterIndex.LowerLeft]);
        ++position.y;
        ApplyGridIndizes(position, _BackGroundInstances[(int)QuarterIndex.UpperLeft]);
        ++position.x;
        ApplyGridIndizes(position, _BackGroundInstances[(int)QuarterIndex.UpperRight]);
        --position.y;
        ApplyGridIndizes(position, _BackGroundInstances[(int)QuarterIndex.LowerRight]);
    }

    void ApplyGridIndizes(IntVector2 gridIndizes, GameObject tile)
    {
        tile.transform.position = new Vector3(gridIndizes.x * backGroundWidth,
                                              gridIndizes.y * backGroundHeight,
                                              backGroundZ);
    }
}
