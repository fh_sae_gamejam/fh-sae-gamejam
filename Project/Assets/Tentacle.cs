﻿using UnityEngine;
using System.Collections;

public enum TentacleState
{
    Grabbing,
    Attached,
    Free,
}

public class Tentacle : MonoBehaviour
{   
    public float range;
    public float grabDuration;
    public TentacleTip tip;

    public TentacleState State {get; private set;}
   
    private Transform _InitialTipParent;
    private bool _InterruptGrabFlag;


	void Awake()
    {
        State = TentacleState.Free;
    }

    void Start()
    {
        _InitialTipParent = tip.transform.parent;
    }

    public bool StartGrab(Vector3 targetPosition)
    {
        _InterruptGrabFlag = false;
        if(State == TentacleState.Free)
        {
            State = TentacleState.Grabbing;
            StartCoroutine(_Grab(targetPosition));
            return true;
        }else
        {
            return false;
        }
    }

    public void InterruptGrab()
    {
        if(State == TentacleState.Grabbing)
        {
            _InterruptGrabFlag = true;
        }
    }

    public void Detach()
    {
        if(State == TentacleState.Attached)
        {
            tip.transform.parent = _InitialTipParent;
            tip.rigidbody.isKinematic = false;
            State = TentacleState.Free;
        }
        if(State == TentacleState.Grabbing)
        {
            InterruptGrab();
        }
    }

    private IEnumerator _Grab(Vector3 targetPosition)
    {
        Vector3 relativeTargetPosition = targetPosition - transform.position;
        float initialTime = Time.timeSinceLevelLoad;
        float actualTime = initialTime;
        RaycastHit sweepTestHit = new RaycastHit();
        while(actualTime < initialTime + grabDuration && !_InterruptGrabFlag)
        {
            /*Collider hit = tip.FirstHit;
            if(hit != null)
            {
                tip.rigidbody.position = hit.transform.position;
                tip.transform.position = hit.transform.position;
                tip.rigidbody.transform.parent = hit.transform;
                tip.rigidbody.isKinematic = true;
                State = TentacleState.Attached;
                break;
            }
            else
            {
                Vector3 sweepTestVector = transform.position - tip.transform.position;
                if(tip.rigidbody.SweepTest(sweepTestVector, out sweepTestHit))
                {
                    if(sweepTestHit.collider.gameObject.layer == 9)
                    {
                        Transform targetTransform = sweepTestHit.collider.transform;
                        tip.rigidbody.position = targetTransform.position;
                        tip.transform.position = hit.transform.position;
                        tip.rigidbody.transform.parent = targetTransform;
                        tip.rigidbody.isKinematic = true;
                        State = TentacleState.Attached;
                    }
                }
            }*/
            actualTime = Time.timeSinceLevelLoad;
            float relativePosition = (actualTime - initialTime) / grabDuration;
            Vector3 actualPosition = transform.position * (1 - relativePosition) + (transform.position + relativeTargetPosition) *  relativePosition;
            if((actualPosition - transform.position).magnitude > range)
            {
                break;
            }
            tip.rigidbody.position = actualPosition;
            yield return null;
        }
        if(State == TentacleState.Grabbing)
        {
            State = TentacleState.Free;
        }
    }


}
