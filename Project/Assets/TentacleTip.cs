﻿using UnityEngine;
using System.Collections.Generic;

public class TentacleTip : MonoBehaviour {

    public Collider FirstHit
    {
        get
        {
            Collider stillColliding = null;
            foreach(Collider collider in _Enters)
            {
                if(!_Exits.Contains(collider))
                {
                    stillColliding = collider;
                }
            }
            _Enters.Clear();
            _Exits.Clear();
            return stillColliding;
        }
    }

    private List<Collider> _Enters;
    private List<Collider> _Exits;

    void Awake()
    {
        _Enters = new List<Collider>();
        _Exits = new List<Collider>();
    }

	void OnTriggerEnter(Collider other)
    {
        if(!_Enters.Contains(other))
        {
            _Enters.Add(other);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(!_Exits.Contains(other))
        {
            _Exits.Add(other);
        }
    }
}
